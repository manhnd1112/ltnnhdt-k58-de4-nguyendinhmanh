/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import Config.Config;
import Helper.MyDate;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import product.BaseProduct;
import product.HoaDon;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class FormHoaDon extends javax.swing.JInternalFrame implements TimKiem, LocThoiGian {

    /**
     * Creates new form FormHoaDon
     */
    FormMain frMain;
    public String locThoiGian = "TOAN_THOI_GIAN";
    public Date date1;
    public Date date2;

    public FormHoaDon(FormMain frMain) {
        try {
            initComponents();
            this.setLocation(-5, -27);
            this.frMain = frMain;
            this.setVisible(true);
            UIManager.setLookAndFeel(Config.windowsClassName);
            SwingUtilities.updateComponentTreeUI(this);
            jDateChooser1.setDate(new Date());
            jDateChooser2.setDate(new Date());
            setViewTable();
            setTableData();
        } catch (Exception ex) {
            Logger.getLogger(FormHoaDon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setViewTable() {
        Font font = new Font("sans-serif", Font.BOLD, 14);
        tableListHoaDon.getTableHeader().setFont(font);
        tableListHoaDon.getTableHeader().setForeground(Color.red);
        tableListHoaDon.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableListHoaDon.getColumnModel().getColumn(0).setPreferredWidth(125);
        tableListHoaDon.getColumnModel().getColumn(1).setPreferredWidth(125);
        tableListHoaDon.getColumnModel().getColumn(2).setPreferredWidth(250);
        tableListHoaDon.getColumnModel().getColumn(3).setPreferredWidth(150);
        tableListHoaDon.getColumnModel().getColumn(4).setPreferredWidth(365);

        Font font1 = new Font("sans-serif", Font.BOLD, 14);
        tableChiTiet.getTableHeader().setFont(font1);
        tableChiTiet.getTableHeader().setForeground(Color.blue);
        tableChiTiet.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableChiTiet.getColumnModel().getColumn(0).setPreferredWidth(100);
        tableChiTiet.getColumnModel().getColumn(1).setPreferredWidth(240);
        tableChiTiet.getColumnModel().getColumn(2).setPreferredWidth(80);
        tableChiTiet.getColumnModel().getColumn(3).setPreferredWidth(80);
        tableChiTiet.getColumnModel().getColumn(4).setPreferredWidth(110);
    }

    public void setTableData() {
        ArrayList<HoaDon> listHD = new ArrayList<>();
        HoaDon hoaDon;
        resetTableData(tableListHoaDon);
        DefaultTableModel model = (DefaultTableModel) tableListHoaDon.getModel();
        for (Map.Entry<String, HoaDon> entrySet : frMain.model.getListHoaDon().entrySet()) {
            hoaDon = entrySet.getValue();
            listHD.add(hoaDon);
            //model.addRow(new Object[]{hoaDon.maHD, hoaDon.ngayLapHoaDon, hoaDon.tenKhachHang, hoaDon.sdtKhachHang, hoaDon.emailKhachHang});
        }

        switch (locThoiGian) {
            case "TOAN_THOI_GIAN":
                for (int i = 0; i < listHD.size(); i++) {
                    hoaDon = listHD.get(i);
                    model.addRow(new Object[]{hoaDon.getMa(), hoaDon.getNgayLap(), hoaDon.getTenKhachHang(), hoaDon.getSdtKhachHang(), hoaDon.getEmailKhachHang()});
                }
                break;
            case "THANG_NAY":
                for (int i = 0; i < listHD.size(); i++) {
                    hoaDon = listHD.get(i);
                    if (MyDate.getMonth(hoaDon.getNgayLap()) == MyDate.getCurrentMonth()) {
                        model.addRow(new Object[]{hoaDon.getMa(), hoaDon.getNgayLap(), hoaDon.getTenKhachHang(), hoaDon.getSdtKhachHang(), hoaDon.getEmailKhachHang()});
                    }
                }
                break;
            case "THANG_TRUOC":
                for (int i = 0; i < listHD.size(); i++) {
                    hoaDon = listHD.get(i);
                    if (MyDate.getMonth(hoaDon.getNgayLap()) == MyDate.getPrevMonth()) {
                        model.addRow(new Object[]{hoaDon.getMa(), hoaDon.getNgayLap(), hoaDon.getTenKhachHang(), hoaDon.getSdtKhachHang(), hoaDon.getEmailKhachHang()});
                        if (MyDate.getPrevMonth() == 12) {
                            if (MyDate.getYear(hoaDon.getNgayLap()) == (MyDate.getCurrentYear() - 1)) {
                                model.addRow(new Object[]{hoaDon.getMa(), hoaDon.getNgayLap(), hoaDon.getTenKhachHang(), hoaDon.getSdtKhachHang(), hoaDon.getEmailKhachHang()});
                            }
                        } else {
                            model.addRow(new Object[]{hoaDon.getMa(), hoaDon.getNgayLap(), hoaDon.getTenKhachHang(), hoaDon.getSdtKhachHang(), hoaDon.getEmailKhachHang()});
                        }
                    }
                }
                break;
            case "LUA_CHON_KHAC":
                for (int i = 0; i < listHD.size(); i++) {
                    hoaDon = listHD.get(i);
                    Date date = MyDate.getDateFromString(hoaDon.getNgayLap());
                    if (MyDate.compareDate(date1, date) <= 0 && MyDate.compareDate(date2, date) >= 0) {
                        model.addRow(new Object[]{hoaDon.getMa(), hoaDon.getNgayLap(), hoaDon.getTenKhachHang(), hoaDon.getSdtKhachHang(), hoaDon.getEmailKhachHang()});
                    }
                }
                break;
            default:
                break;
        }
    }

    public void resetTableData(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void setInfoChiTiet(HoaDon hoaDon) {
        if (hoaDon != null) {
            this.labelMaHD.setText(hoaDon.getMa());
            this.labelNgayLap.setText(hoaDon.getNgayLap());
            this.labelTenKH.setText(hoaDon.getTenKhachHang());
            this.labelSDT.setText(hoaDon.getSdtKhachHang());
            this.labelEmail.setText(hoaDon.getEmailKhachHang());
            // Thông tin chi tiết
            resetTableData(tableChiTiet);
            DefaultTableModel model = (DefaultTableModel) tableChiTiet.getModel();
            int tongTien = 0;
            int tongSoLuongSP = 0;
            for (int i = 0; i < hoaDon.getListChiTiet().size(); i++) {
                String maSP = hoaDon.getListChiTiet().get(i).getMaSP();
                String tenSP = "";
                int donGia = hoaDon.getListChiTiet().get(i).getDonGia();
                int soLuong = 0;
                int thanhTien = 0;
                Object obj = frMain.model.getByKey(maSP);
                if (obj != null) {
                    BaseProduct pro = (BaseProduct) obj;
                    tenSP = pro.getTenSP();
                    soLuong = hoaDon.getListChiTiet().get(i).getSoLuong();
                    thanhTien = soLuong * donGia;
                    model.addRow(new Object[]{maSP, tenSP, donGia, soLuong, thanhTien});
                }
                tongTien += thanhTien;
                tongSoLuongSP += soLuong;
            }
            this.labelTongSP.setText(tongSoLuongSP + "");
            this.labelTongTien.setText(tongTien + "");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btGroupLocThoiGian = new javax.swing.ButtonGroup();
        panelLocHoaDon = new javax.swing.JPanel();
        panelNhomHang3 = new javax.swing.JPanel();
        labelTitle3 = new javax.swing.JLabel();
        tfSearch = new javax.swing.JTextField();
        panelNhomHang5 = new javax.swing.JPanel();
        labelTitle5 = new javax.swing.JLabel();
        radioThangNay = new javax.swing.JRadioButton();
        radioToanThoiGian = new javax.swing.JRadioButton();
        radioThangTruoc = new javax.swing.JRadioButton();
        radioLuaChonKhac = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jSeparator2 = new javax.swing.JSeparator();
        panelSanPham = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableListHoaDon = new javax.swing.JTable();
        btThemHD = new javax.swing.JButton();
        panelMain = new javax.swing.JPanel();
        panelBaseInfo = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        labelTenKH = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelEmail = new javax.swing.JLabel();
        labelMaHD = new javax.swing.JLabel();
        labelNgayLap = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        labelSDT = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        panelChiTiet1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableChiTiet = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        labelTongTien = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        labelTongSP = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        labelTitle3.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle3.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle3.setText("   Tìm kiếm");
        labelTitle3.setAlignmentX(1.0F);
        labelTitle3.setOpaque(true);

        tfSearch.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tfSearch.setText("Tìm theo mã hóa đơn");
        tfSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfSearchMouseClicked(evt);
            }
        });
        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfSearchKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang3Layout = new javax.swing.GroupLayout(panelNhomHang3);
        panelNhomHang3.setLayout(panelNhomHang3Layout);
        panelNhomHang3Layout.setHorizontalGroup(
            panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang3Layout.createSequentialGroup()
                .addGroup(panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panelNhomHang3Layout.setVerticalGroup(
            panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelTitle5.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle5.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle5.setText("   Lọc thời gian");
        labelTitle5.setAlignmentX(1.0F);
        labelTitle5.setOpaque(true);

        btGroupLocThoiGian.add(radioThangNay);
        radioThangNay.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioThangNay.setText("Tháng này");
        radioThangNay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        btGroupLocThoiGian.add(radioToanThoiGian);
        radioToanThoiGian.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioToanThoiGian.setSelected(true);
        radioToanThoiGian.setText("Toàn thời gian");
        radioToanThoiGian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        btGroupLocThoiGian.add(radioThangTruoc);
        radioThangTruoc.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioThangTruoc.setText("Tháng trước");
        radioThangTruoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        btGroupLocThoiGian.add(radioLuaChonKhac);
        radioLuaChonKhac.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioLuaChonKhac.setText("Lựa chọn khác");
        radioLuaChonKhac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel1.setText("Từ ngày:");

        jDateChooser1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateChange(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel7.setText("Đến ngày");

        jDateChooser2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateChange(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang5Layout = new javax.swing.GroupLayout(panelNhomHang5);
        panelNhomHang5.setLayout(panelNhomHang5Layout);
        panelNhomHang5Layout.setHorizontalGroup(
            panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang5Layout.createSequentialGroup()
                .addGroup(panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(radioThangTruoc, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(radioToanThoiGian, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(radioThangNay, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelTitle5, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(radioLuaChonKhac, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelNhomHang5Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelNhomHang5Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panelNhomHang5Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panelNhomHang5Layout.setVerticalGroup(
            panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTitle5, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioToanThoiGian, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioThangNay, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioThangTruoc, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioLuaChonKhac, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelLocHoaDonLayout = new javax.swing.GroupLayout(panelLocHoaDon);
        panelLocHoaDon.setLayout(panelLocHoaDonLayout);
        panelLocHoaDonLayout.setHorizontalGroup(
            panelLocHoaDonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocHoaDonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelNhomHang3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLocHoaDonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelNhomHang5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelLocHoaDonLayout.setVerticalGroup(
            panelLocHoaDonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocHoaDonLayout.createSequentialGroup()
                .addComponent(panelNhomHang3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelNhomHang5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        tableListHoaDon.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        tableListHoaDon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Mã hóa đơn", "Ngày lập", "Tên khách hàng", "Số điện thoại", "Email"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tableListHoaDon.setToolTipText("");
        tableListHoaDon.setAlignmentX(1.0F);
        tableListHoaDon.setRowHeight(25);
        tableListHoaDon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableListHoaDonMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableListHoaDon);

        btThemHD.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btThemHD.setForeground(new java.awt.Color(51, 51, 51));
        btThemHD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/plus_icon.png"))); // NOI18N
        btThemHD.setText("Bán hàng");
        btThemHD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btThemHDActionPerformed(evt);
            }
        });

        panelMain.setBackground(new java.awt.Color(255, 255, 255));

        panelBaseInfo.setBackground(new java.awt.Color(255, 255, 255));
        panelBaseInfo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel4.setText("Ngày lập hóa đơn");

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel5.setText("Mã hóa đơn:");

        labelTenKH.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel6.setText("Số Điện thoại khách hàng");

        labelEmail.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        labelMaHD.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        labelNgayLap.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        jLabel10.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel10.setText("Tên Khách hàng");

        labelSDT.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        jLabel12.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel12.setText("Email khách hàng");

        javax.swing.GroupLayout panelBaseInfoLayout = new javax.swing.GroupLayout(panelBaseInfo);
        panelBaseInfo.setLayout(panelBaseInfoLayout);
        panelBaseInfoLayout.setHorizontalGroup(
            panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelMaHD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelNgayLap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelTenKH, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelSDT, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                    .addComponent(labelEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelBaseInfoLayout.setVerticalGroup(
            panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfoLayout.createSequentialGroup()
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelMaHD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(9, 9, 9)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNgayLap, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTenKH, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSDT, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        panelChiTiet1.setBackground(new java.awt.Color(255, 255, 255));

        tableChiTiet.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tableChiTiet.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Mã sản phẩm", "Tên sản phẩm", "Đơn giá", "Số lượng", "Thành tiền"
            }
        ));
        tableChiTiet.setRowHeight(25);
        jScrollPane2.setViewportView(tableChiTiet);

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 204, 204));
        jLabel2.setText("Tổng sản phẩm");

        labelTongTien.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        labelTongTien.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTongTien.setText("0");
        labelTongTien.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 204, 204));
        jLabel3.setText("Tổng tiền hàng");

        labelTongSP.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        labelTongSP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTongSP.setText("0");
        labelTongSP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));

        javax.swing.GroupLayout panelChiTiet1Layout = new javax.swing.GroupLayout(panelChiTiet1);
        panelChiTiet1.setLayout(panelChiTiet1Layout);
        panelChiTiet1Layout.setHorizontalGroup(
            panelChiTiet1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChiTiet1Layout.createSequentialGroup()
                .addGap(0, 5, Short.MAX_VALUE)
                .addGroup(panelChiTiet1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelChiTiet1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(panelChiTiet1Layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelTongSP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelChiTiet1Layout.createSequentialGroup()
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 619, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        panelChiTiet1Layout.setVerticalGroup(
            panelChiTiet1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChiTiet1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelChiTiet1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTongSP, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelChiTiet1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        jButton1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/print.png"))); // NOI18N
        jButton1.setText("In ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelBaseInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelChiTiet1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(panelBaseInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelChiTiet1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout panelSanPhamLayout = new javax.swing.GroupLayout(panelSanPham);
        panelSanPham.setLayout(panelSanPhamLayout);
        panelSanPhamLayout.setHorizontalGroup(
            panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSanPhamLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelSanPhamLayout.createSequentialGroup()
                        .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 7, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSanPhamLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btThemHD, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        panelSanPhamLayout.setVerticalGroup(
            panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSanPhamLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btThemHD)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelLocHoaDon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelSanPham, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelLocHoaDon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelSanPham, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btThemHDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btThemHDActionPerformed
        // TODO add your handling code here:
        frMain.setFormPanelMain("ADD HÓA ĐƠN");
    }//GEN-LAST:event_btThemHDActionPerformed

    private void tableListHoaDonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableListHoaDonMouseClicked
        // TODO add your handling code here:
        String maHD = tableListHoaDon.getValueAt(tableListHoaDon.getSelectedRow(), 0).toString();
        Object obj = frMain.model.getByKey(maHD);
        if (obj != null && (obj instanceof HoaDon)) {
            setInfoChiTiet((HoaDon) obj);
        }
    }//GEN-LAST:event_tableListHoaDonMouseClicked

    private void LocThoiGian(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocThoiGian
        // TODO add your handling code here:
        LocThoiGian();
    }//GEN-LAST:event_LocThoiGian

    private void DateChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_DateChange
        // TODO add your handling code here:
        date1 = jDateChooser1.getDate();
        date2 = jDateChooser2.getDate();
        if (locThoiGian.equals("LUA_CHON_KHAC") == true) {
            setTableData();
        }
    }//GEN-LAST:event_DateChange

    private void tfSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            TimKiem(tfSearch.getText());
        }
    }//GEN-LAST:event_tfSearchKeyPressed

    private void tfSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfSearchMouseClicked
        // TODO add your handling code here:
        tfSearch.selectAll();
    }//GEN-LAST:event_tfSearchMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String maHD = labelMaHD.getText();
        if (maHD == null || maHD.equals("") == true) {
            JOptionPane.showMessageDialog(null, "Lỗi! Bạn chưa chọn hóa đơn", "Thông báo", JOptionPane.ERROR_MESSAGE);
        } else {
            HoaDon hd = (HoaDon) frMain.model.getByKey(maHD);
            FormInHoaDon frInHD = new FormInHoaDon(frMain, hd);
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btGroupLocThoiGian;
    private javax.swing.JButton btThemHD;
    private javax.swing.JButton jButton1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel labelEmail;
    private javax.swing.JLabel labelMaHD;
    private javax.swing.JLabel labelNgayLap;
    private javax.swing.JLabel labelSDT;
    private javax.swing.JLabel labelTenKH;
    private javax.swing.JLabel labelTitle3;
    private javax.swing.JLabel labelTitle5;
    private javax.swing.JLabel labelTongSP;
    private javax.swing.JLabel labelTongTien;
    private javax.swing.JPanel panelBaseInfo;
    private javax.swing.JPanel panelChiTiet1;
    private javax.swing.JPanel panelLocHoaDon;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelNhomHang3;
    private javax.swing.JPanel panelNhomHang5;
    private javax.swing.JPanel panelSanPham;
    private javax.swing.JRadioButton radioLuaChonKhac;
    private javax.swing.JRadioButton radioThangNay;
    private javax.swing.JRadioButton radioThangTruoc;
    private javax.swing.JRadioButton radioToanThoiGian;
    private javax.swing.JTable tableChiTiet;
    private javax.swing.JTable tableListHoaDon;
    private javax.swing.JTextField tfSearch;
    // End of variables declaration//GEN-END:variables

    @Override
    public void TimKiem(String textSearch) {
        DefaultTableModel model = (DefaultTableModel) tableListHoaDon.getModel();
        for (int i = (model.getRowCount() - 1); i >= 0; i--) {
            String maHD = tableListHoaDon.getValueAt(i, 0).toString();
            if (maHD.equalsIgnoreCase(textSearch) == false) {
                model.removeRow(i);
            }
        }
    }

    @Override
    public void LocThoiGian() {
        if (radioToanThoiGian.isSelected()) {
            locThoiGian = "TOAN_THOI_GIAN";
        } else if (radioThangNay.isSelected()) {
            locThoiGian = "THANG_NAY";
        } else if (radioThangTruoc.isSelected()) {
            locThoiGian = "THANG_TRUOC";
        } else if (radioLuaChonKhac.isSelected()) {
            locThoiGian = "LUA_CHON_KHAC";
        }
        setTableData();
    }
}
