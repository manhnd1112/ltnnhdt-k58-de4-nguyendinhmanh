/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import Config.Config;
import Helper.Data;
import image.ImageProcess;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import product.BaseProduct;
import product.DiaNhac;
import product.DiaPhim;
import product.Sach;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class FormSanPham extends javax.swing.JInternalFrame implements TimKiem, LocNhomSanPham {

    /**
     * Creates new form FormSanPham frMain: FormMain chính nhomSP: Thể hiện
     * nhóm sản phẩm đang hiển thị Có 4 lựa chọn: ALL (Tất cả), NHAC
     * (chỉ đĩa nhạc), PHIM (Chỉđĩa phim) hoặc SACH(Chỉ sách) Mặc đinh
     * là ALL: tức là tất cả sản phẩm locTonKho: Thể hiện việc có
     * lọc theo tồn kho hay không? Có 3 lựa chọn: KHONG_LOC(không lọc),
     * CON_HANG(còn hàng), HET_HANG(hết hàng) Mặc đinh la: KHONG_LOC̀
     */
    public FormMain frMain;

    public String nhomSP = "ALL";
    public String locTonKho = "KHONG_LOC";

    public FormSanPham(FormMain frMain) {
        try {
            initComponents();
            this.setLocation(-5, -27);
            this.frMain = frMain;
            UIManager.setLookAndFeel(Config.windowsClassName);
            SwingUtilities.updateComponentTreeUI(this);
            // Hiển thị dữ liệu
            setViewTable();
            // Hiển thị theo nhóm sản phẩm: mặc định là tất cả(ALL)
            setTableData();

        } catch (Exception ex) {
            Logger.getLogger(FormSanPham.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setTableData() {
        ArrayList<BaseProduct> listSP = new ArrayList<>();
        BaseProduct pro;
        resetTableData(tableListSP);
        DefaultTableModel model = (DefaultTableModel) tableListSP.getModel();
        switch (nhomSP) {
            case "ALL":
                for (Map.Entry<String, BaseProduct> entrySet : frMain.model.getListPro().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                    //model.addRow(new Object[]{pro.maSP, pro.tenSP, pro.giaBan, pro.soluong});
                }
                break;
            case "NHAC":
                for (Map.Entry<String, DiaNhac> entrySet : frMain.model.getListDiaNhac().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                    //model.addRow(new Object[]{pro.maSP, pro.tenSP, pro.giaBan, pro.soluong});
                }
                break;
            case "PHIM":
                for (Map.Entry<String, DiaPhim> entrySet : frMain.model.getListDiaPhim().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                    //model.addRow(new Object[]{pro.maSP, pro.tenSP, pro.giaBan, pro.soluong});
                }
                break;
            case "SACH":
                for (Map.Entry<String, Sach> entrySet : frMain.model.getListSach().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                    //model.addRow(new Object[]{pro.maSP, pro.tenSP, pro.giaBan, pro.soluong});
                }
                break;
            default:
                break;
        }

        switch (locTonKho) {
            case "KHONG_LOC":
                for (int i = 0; i < listSP.size(); i++) {
                    pro = listSP.get(i);
                    model.addRow(new Object[]{pro.getMaSP(), pro.getTenSP(), pro.getGiaBan(), pro.getSoluong()});
                }
                break;
            case "CON_HANG":
                for (int i = 0; i < listSP.size(); i++) {
                    pro = listSP.get(i);
                    if (pro.getSoluong() > 0) {
                        model.addRow(new Object[]{pro.getMaSP(), pro.getTenSP(), pro.getGiaBan(), pro.getSoluong()});
                    }
                }
                break;
            case "HET_HANG":
                for (int i = 0; i < listSP.size(); i++) {
                    pro = listSP.get(i);
                    if (pro.getSoluong() <= 0) {
                        model.addRow(new Object[]{pro.getMaSP(), pro.getTenSP(), pro.getGiaBan(), pro.getSoluong()});
                    }
                }
                break;
            default:
                break;
        }

        // Set lại Phần chi tiết sản phẩm
        String maSP = this.labelMaSP.getText();
        Object obj = frMain.model.getByKey(maSP);
        if (obj != null) {
            setInfoChiTietSP(obj);
        } else {
            resetInfoChiTietSP();
        }
    }

    public void resetTableData(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void setViewTable() {
        Font font = new Font("sans-serif", Font.BOLD, 14);
        tableListSP.getTableHeader().setFont(font);
        tableListSP.getTableHeader().setForeground(Color.red);
        tableListSP.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableListSP.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableListSP.getColumnModel().getColumn(1).setPreferredWidth(500);
        tableListSP.getColumnModel().getColumn(2).setPreferredWidth(170);
        tableListSP.getColumnModel().getColumn(3).setPreferredWidth(160);
    }

    public void setInfoChiTietSP(Object obj) {
        if (obj != null) {
            BaseProduct pro = (BaseProduct) obj;
            labelMaSP.setText(pro.getMaSP());
            labelTenSP.setText(pro.getTenSP());
            if (pro instanceof DiaPhim) {
                labelLoaiSP.setText("Đĩa Phim");
            } else if (pro instanceof DiaNhac) {
                labelLoaiSP.setText("Đĩa Nhạc");
            } else if (pro instanceof Sach) {
                labelLoaiSP.setText("Sách");
            }
            labelSLTon.setText(pro.getSoluong() + "");
            labelGiaBan.setText(pro.getGiaBan() + "");
            ImageProcess.setLabelIcon(labelAvatar, pro.getLinkAvatar(), 227, 193);
            String chiTiet = new String();
            chiTiet += "<html>";
            if (pro instanceof DiaNhac) {
                DiaNhac dn = (DiaNhac) pro;
                chiTiet += "<br><b>Thể loại nhạc: </b>" + dn.getTheLoai();
                chiTiet += "<br><br><br><b>Các ca sĩ: </b>";
                for (int i = 0; i < dn.getListCasi().size(); i++) {
                    chiTiet += dn.getListCasi().get(i) + ", ";
                }
                chiTiet += "<br><br><br><b>Danh sách bài hát: </b><br>";
                chiTiet += "<ul style=\"list-style-type: none\">";
                for (int i = 0; i < dn.getListBaiHat().size(); i++) {
                    chiTiet += "<li>" + dn.getListBaiHat().get(i) + "</li>";
                }
                chiTiet += "</ul>";
            } else if (pro instanceof DiaPhim) {
                DiaPhim dp = (DiaPhim) pro;
                chiTiet += "<br><b>Thể loại phim:  </b>" + dp.getTheLoai();
                chiTiet += "<br><br><br><b>Các diễn viên:  </b>";
                for (int i = 0; i < dp.getListDienVien().size(); i++) {
                    chiTiet += dp.getListDienVien().get(i) + ",";
                }
                chiTiet += "<br><br><br><b>Đạo diễn:  </b>" + dp.getDaoDien();
            } else if (pro instanceof Sach) {
                Sach sa = (Sach) pro;
                chiTiet += "<br><b>Tác giả: </b>" + sa.getTacGia();
                chiTiet += "<br><br><br><b>Nhà xuất bản: </b>" + sa.getNhaXB();
                chiTiet += "<br><br><br><b>Năm xuất bản: </b>" + sa.getNamXB();
                chiTiet += "<br><br><br><b>Số trang: </b>" + sa.getSoTrang();
            }
            chiTiet += "</html>";
            labelChiTiet.setText(chiTiet);
        }
    }

    public void resetInfoChiTietSP() {
        labelMaSP.setText("");
        labelTenSP.setText("");
        labelLoaiSP.setText("");
        labelSLTon.setText("");
        labelGiaBan.setText("");
        labelChiTiet.setText("");
        ImageProcess.setLabelIcon(labelAvatar, Config.AVATAR_DEFAULT, 227, 193);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btGroupNhomSP = new javax.swing.ButtonGroup();
        btGroupTonKho = new javax.swing.ButtonGroup();
        panelLocHoaDon = new javax.swing.JPanel();
        panelNhomHang3 = new javax.swing.JPanel();
        labelTitle3 = new javax.swing.JLabel();
        tfSearch = new javax.swing.JTextField();
        panelNhomHang5 = new javax.swing.JPanel();
        labelTitle5 = new javax.swing.JLabel();
        radioDiaNhac = new javax.swing.JRadioButton();
        radioTatCa = new javax.swing.JRadioButton();
        radioDiaPhim = new javax.swing.JRadioButton();
        radioSach = new javax.swing.JRadioButton();
        panelNhomHang6 = new javax.swing.JPanel();
        labelTitle6 = new javax.swing.JLabel();
        radioConHang = new javax.swing.JRadioButton();
        radioKhongLocTon = new javax.swing.JRadioButton();
        radioHetHang = new javax.swing.JRadioButton();
        jSeparator3 = new javax.swing.JSeparator();
        panelSanPham = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableListSP = new javax.swing.JTable();
        panelChiTiet = new javax.swing.JPanel();
        panelMain1 = new javax.swing.JPanel();
        panelBaseInfo1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        labelMaSP = new javax.swing.JLabel();
        labelSLTon = new javax.swing.JLabel();
        labelTenSP = new javax.swing.JLabel();
        labelLoaiSP = new javax.swing.JLabel();
        labelGiaBan = new javax.swing.JLabel();
        labelAvatar = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        labelChiTiet = new javax.swing.JLabel();
        btUpdate = new javax.swing.JButton();
        btDel = new javax.swing.JButton();
        btAddProduct = new javax.swing.JButton();

        labelTitle3.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle3.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle3.setText("   Tìm kiếm");
        labelTitle3.setAlignmentX(1.0F);
        labelTitle3.setOpaque(true);

        tfSearch.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tfSearch.setText("Tìm theo mã sản phẩm hoặc tên sản phẩm");
        tfSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfSearchMouseClicked(evt);
            }
        });
        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfSearchKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang3Layout = new javax.swing.GroupLayout(panelNhomHang3);
        panelNhomHang3.setLayout(panelNhomHang3Layout);
        panelNhomHang3Layout.setHorizontalGroup(
            panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelNhomHang3Layout.setVerticalGroup(
            panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelTitle5.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle5.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle5.setText("   Lọc theo nhóm sản phẩm");
        labelTitle5.setAlignmentX(1.0F);
        labelTitle5.setOpaque(true);

        btGroupNhomSP.add(radioDiaNhac);
        radioDiaNhac.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioDiaNhac.setText("Đĩa nhạc");
        radioDiaNhac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        btGroupNhomSP.add(radioTatCa);
        radioTatCa.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioTatCa.setSelected(true);
        radioTatCa.setText("Tất cả");
        radioTatCa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        btGroupNhomSP.add(radioDiaPhim);
        radioDiaPhim.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioDiaPhim.setText("Đĩa phim");
        radioDiaPhim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        btGroupNhomSP.add(radioSach);
        radioSach.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioSach.setText("Sách");
        radioSach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang5Layout = new javax.swing.GroupLayout(panelNhomHang5);
        panelNhomHang5.setLayout(panelNhomHang5Layout);
        panelNhomHang5Layout.setHorizontalGroup(
            panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang5Layout.createSequentialGroup()
                .addGroup(panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(radioSach, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioDiaPhim, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioTatCa, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioDiaNhac, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTitle5, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panelNhomHang5Layout.setVerticalGroup(
            panelNhomHang5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTitle5, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioTatCa, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioDiaNhac, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioDiaPhim, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSach, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelTitle6.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle6.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle6.setText("  Lọc tồn kho");
        labelTitle6.setAlignmentX(1.0F);
        labelTitle6.setOpaque(true);

        btGroupTonKho.add(radioConHang);
        radioConHang.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioConHang.setText("Còn hàng");
        radioConHang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocTonKho(evt);
            }
        });

        btGroupTonKho.add(radioKhongLocTon);
        radioKhongLocTon.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioKhongLocTon.setSelected(true);
        radioKhongLocTon.setText("Không lọc tồn kho");
        radioKhongLocTon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocTonKho(evt);
            }
        });

        btGroupTonKho.add(radioHetHang);
        radioHetHang.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioHetHang.setText("Hết hàng");
        radioHetHang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocTonKho(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang6Layout = new javax.swing.GroupLayout(panelNhomHang6);
        panelNhomHang6.setLayout(panelNhomHang6Layout);
        panelNhomHang6Layout.setHorizontalGroup(
            panelNhomHang6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang6Layout.createSequentialGroup()
                .addGroup(panelNhomHang6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(radioHetHang, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioKhongLocTon, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioConHang, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTitle6, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panelNhomHang6Layout.setVerticalGroup(
            panelNhomHang6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTitle6, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioKhongLocTon, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioConHang, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioHetHang, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(56, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelLocHoaDonLayout = new javax.swing.GroupLayout(panelLocHoaDon);
        panelLocHoaDon.setLayout(panelLocHoaDonLayout);
        panelLocHoaDonLayout.setHorizontalGroup(
            panelLocHoaDonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocHoaDonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLocHoaDonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLocHoaDonLayout.createSequentialGroup()
                        .addComponent(panelNhomHang3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 8, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLocHoaDonLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panelLocHoaDonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelNhomHang5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelNhomHang6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        panelLocHoaDonLayout.setVerticalGroup(
            panelLocHoaDonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocHoaDonLayout.createSequentialGroup()
                .addComponent(panelNhomHang3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelNhomHang5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelNhomHang6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        tableListSP.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        tableListSP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã sản phẩm", "Tên sản phẩm", "Giá bán", "Số lượng tồn"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tableListSP.setToolTipText("");
        tableListSP.setAlignmentX(1.0F);
        tableListSP.setRowHeight(25);
        tableListSP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableListSPMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableListSP);

        panelChiTiet.setBackground(new java.awt.Color(255, 255, 255));

        panelMain1.setBackground(new java.awt.Color(255, 255, 255));

        panelBaseInfo1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel9.setText("Loại sản phẩm:");

        jLabel10.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel10.setText("Mã sản phẩm:");

        jLabel11.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel11.setText("Tên sản phẩm:");

        jLabel12.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel12.setText("Giá bán:");

        jLabel13.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel13.setText("Số lượng tồn:");

        labelMaSP.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N

        labelSLTon.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N

        labelTenSP.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        labelTenSP.setToolTipText("");

        labelLoaiSP.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N

        labelGiaBan.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N

        javax.swing.GroupLayout panelBaseInfo1Layout = new javax.swing.GroupLayout(panelBaseInfo1);
        panelBaseInfo1.setLayout(panelBaseInfo1Layout);
        panelBaseInfo1Layout.setHorizontalGroup(
            panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfo1Layout.createSequentialGroup()
                .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(panelBaseInfo1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelBaseInfo1Layout.createSequentialGroup()
                            .addGap(19, 19, 19)
                            .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelMaSP, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSLTon, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTenSP, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelLoaiSP, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelGiaBan, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelBaseInfo1Layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(222, Short.MAX_VALUE)))
        );
        panelBaseInfo1Layout.setVerticalGroup(
            panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfo1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(labelMaSP, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTenSP, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelLoaiSP, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSLTon, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelGiaBan, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelBaseInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelBaseInfo1Layout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(371, Short.MAX_VALUE)))
        );

        labelAvatar.setBackground(new java.awt.Color(204, 204, 255));
        labelAvatar.setOpaque(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        labelChiTiet.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        labelChiTiet.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(labelChiTiet, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelChiTiet, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        btUpdate.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Update.png"))); // NOI18N
        btUpdate.setText("Cập nhật");
        btUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUpdatebuttonAction(evt);
            }
        });

        btDel.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Delete.png"))); // NOI18N
        btDel.setText("Xóa");
        btDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDelbuttonAction(evt);
            }
        });

        javax.swing.GroupLayout panelMain1Layout = new javax.swing.GroupLayout(panelMain1);
        panelMain1.setLayout(panelMain1Layout);
        panelMain1Layout.setHorizontalGroup(
            panelMain1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMain1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMain1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMain1Layout.createSequentialGroup()
                        .addComponent(btUpdate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDel, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(panelBaseInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelMain1Layout.setVerticalGroup(
            panelMain1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMain1Layout.createSequentialGroup()
                .addGroup(panelMain1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMain1Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(labelAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(panelMain1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btUpdate)
                            .addComponent(btDel)))
                    .addGroup(panelMain1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelMain1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelBaseInfo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelChiTietLayout = new javax.swing.GroupLayout(panelChiTiet);
        panelChiTiet.setLayout(panelChiTietLayout);
        panelChiTietLayout.setHorizontalGroup(
            panelChiTietLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChiTietLayout.createSequentialGroup()
                .addComponent(panelMain1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelChiTietLayout.setVerticalGroup(
            panelChiTietLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChiTietLayout.createSequentialGroup()
                .addComponent(panelMain1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 46, Short.MAX_VALUE))
        );

        btAddProduct.setBackground(new java.awt.Color(255, 255, 255));
        btAddProduct.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btAddProduct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/plus_icon.png"))); // NOI18N
        btAddProduct.setText("Thêm mới");
        btAddProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddProductActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelSanPhamLayout = new javax.swing.GroupLayout(panelSanPham);
        panelSanPham.setLayout(panelSanPhamLayout);
        panelSanPhamLayout.setHorizontalGroup(
            panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSanPhamLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btAddProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56))
            .addGroup(panelSanPhamLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelChiTiet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 999, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        panelSanPhamLayout.setVerticalGroup(
            panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSanPhamLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(btAddProduct)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelChiTiet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(49, 49, 49))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelLocHoaDon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelSanPham, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelLocHoaDon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelSanPham, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator3))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableListSPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableListSPMouseClicked
        String maSP = tableListSP.getValueAt(tableListSP.getSelectedRow(), 0).toString();
        Object obj = frMain.model.getByKey(maSP);
        if (obj instanceof BaseProduct) {
            setInfoChiTietSP(obj);
        }
    }//GEN-LAST:event_tableListSPMouseClicked

    private void btAddProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddProductActionPerformed
        // TODO add your handling code here:
        FormChooseType frType = new FormChooseType(this.frMain, this);
    }//GEN-LAST:event_btAddProductActionPerformed

    private void btUpdatebuttonAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUpdatebuttonAction
        // TODO add your handling code here:
        String maSP = labelMaSP.getText();
        if (frMain.model.getListPro().containsKey(maSP)) {
            BaseProduct pro = frMain.model.getListPro().get(maSP);
            if (pro instanceof DiaNhac) {
                FormAddEditDiaNhac f = new FormAddEditDiaNhac(this.frMain, pro);
            }
            if (pro instanceof DiaPhim) {
                FormAddEditDiaPhim f = new FormAddEditDiaPhim(this.frMain, pro);
            }
            if (pro instanceof Sach) {
                FormAddEditSach f = new FormAddEditSach(this.frMain, pro);
            }
        }
    }//GEN-LAST:event_btUpdatebuttonAction

    private void btDelbuttonAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDelbuttonAction
        // TODO add your handling code here:
        String maSP = labelMaSP.getText();

        int choose = JOptionPane.showConfirmDialog(null, " Sẽ xóa toàn bộ những thông tin liên quan đến sản phẩm!\nBạn có chắc muốn xóa sản phẩm này?", "Xóa sản phẩm", JOptionPane.YES_NO_CANCEL_OPTION);
        if (choose == JOptionPane.YES_OPTION) {
            this.frMain.model.delByKey(maSP);
            resetInfoChiTietSP();
            frMain.UpdateAll();
            JOptionPane.showMessageDialog(null, "Xóa thành công!", "Thông báo", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btDelbuttonAction

    private void LocNhomSP(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocNhomSP
        // TODO add your handling code here:
        LocNhomSP();
    }//GEN-LAST:event_LocNhomSP

    private void LocTonKho(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocTonKho
        // TODO add your handling code here:
        if (radioKhongLocTon.isSelected()) {
            locTonKho = "KHONG_LOC";
            setTableData();
        } else if (radioConHang.isSelected()) {
            locTonKho = "CON_HANG";
            setTableData();
        } else if (radioHetHang.isSelected()) {
            locTonKho = "HET_HANG";
            setTableData();
        }
    }//GEN-LAST:event_LocTonKho

    private void tfSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyPressed
        // TODO add your handling code here:
        String textSearch = tfSearch.getText();
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            TimKiem(textSearch);
        }
    }//GEN-LAST:event_tfSearchKeyPressed

    private void tfSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfSearchMouseClicked
        // TODO add your handling code here:
        tfSearch.selectAll();
    }//GEN-LAST:event_tfSearchMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddProduct;
    private javax.swing.JButton btDel;
    private javax.swing.ButtonGroup btGroupNhomSP;
    private javax.swing.ButtonGroup btGroupTonKho;
    private javax.swing.JButton btUpdate;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel labelAvatar;
    private javax.swing.JLabel labelChiTiet;
    private javax.swing.JLabel labelGiaBan;
    private javax.swing.JLabel labelLoaiSP;
    public javax.swing.JLabel labelMaSP;
    private javax.swing.JLabel labelSLTon;
    private javax.swing.JLabel labelTenSP;
    private javax.swing.JLabel labelTitle3;
    private javax.swing.JLabel labelTitle5;
    private javax.swing.JLabel labelTitle6;
    private javax.swing.JPanel panelBaseInfo1;
    public javax.swing.JPanel panelChiTiet;
    private javax.swing.JPanel panelLocHoaDon;
    private javax.swing.JPanel panelMain1;
    private javax.swing.JPanel panelNhomHang3;
    private javax.swing.JPanel panelNhomHang5;
    private javax.swing.JPanel panelNhomHang6;
    private javax.swing.JPanel panelSanPham;
    private javax.swing.JRadioButton radioConHang;
    private javax.swing.JRadioButton radioDiaNhac;
    private javax.swing.JRadioButton radioDiaPhim;
    private javax.swing.JRadioButton radioHetHang;
    private javax.swing.JRadioButton radioKhongLocTon;
    private javax.swing.JRadioButton radioSach;
    private javax.swing.JRadioButton radioTatCa;
    private javax.swing.JTable tableListSP;
    private javax.swing.JTextField tfSearch;
    // End of variables declaration//GEN-END:variables

    @Override
    public void TimKiem(String textSearch) {
        DefaultTableModel model = (DefaultTableModel) tableListSP.getModel();
        for (int i = (model.getRowCount() - 1); i >= 0; i--) {
            String maSP = tableListSP.getValueAt(i, 0).toString();
            String tenSP = tableListSP.getValueAt(i, 1).toString();
            if (maSP.equalsIgnoreCase(textSearch) == false && tenSP.toUpperCase().contains(textSearch.toUpperCase()) == false && tenSP.equalsIgnoreCase(textSearch) == false) {
                model.removeRow(i);
            }
        }
    }

    @Override
    public void LocNhomSP() {
        if (radioTatCa.isSelected()) {
            nhomSP = "ALL";
        } else if (radioDiaNhac.isSelected()) {
            nhomSP = "NHAC";
        } else if (radioDiaPhim.isSelected()) {
            nhomSP = "PHIM";
        } else if (radioSach.isSelected()) {
            nhomSP = "SACH";
        }
        setTableData();
    }
}
