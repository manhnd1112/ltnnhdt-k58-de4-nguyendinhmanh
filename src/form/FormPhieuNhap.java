/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import Config.Config;
import Helper.MyDate;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import product.BaseProduct;
import product.PhieuNhap;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class FormPhieuNhap extends javax.swing.JInternalFrame implements TimKiem, LocThoiGian {

    /**
     * Creates new form FormPhieuNhap
     */
    public FormMain frMain;
    public HashMap<String, PhieuNhap> listPhieuNhap = new HashMap<>();
    public String locTrangThai = "ALL";
    public String locThoiGian = "TOAN_THOI_GIAN";
    public Date date1;
    public Date date2;

    public FormPhieuNhap(FormMain frMain) {
        try {
            initComponents();
            this.setLocation(-5, -27);
            this.frMain = frMain;
            UIManager.setLookAndFeel(Config.windowsClassName);
            SwingUtilities.updateComponentTreeUI(this);
            jDateChooser1.setDate(new Date());
            jDateChooser2.setDate(new Date());
            setViewTable();
            setTableData();

        } catch (Exception ex) {
            Logger.getLogger(FormPhieuNhap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setViewTable() {
        Font font = new Font("sans-serif", Font.BOLD, 14);
        tableListPhieuNhap.getTableHeader().setFont(font);
        tableListPhieuNhap.getTableHeader().setForeground(Color.red);
        tableListPhieuNhap.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableListPhieuNhap.getColumnModel().getColumn(0).setPreferredWidth(220);
        tableListPhieuNhap.getColumnModel().getColumn(1).setPreferredWidth(220);
        tableListPhieuNhap.getColumnModel().getColumn(2).setPreferredWidth(360);
        tableListPhieuNhap.getColumnModel().getColumn(3).setPreferredWidth(220);

        Font font1 = new Font("sans-serif", Font.BOLD, 14);
        tableChiTiet.getTableHeader().setFont(font1);
        tableChiTiet.getTableHeader().setForeground(Color.blue);
        tableChiTiet.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableChiTiet.getColumnModel().getColumn(0).setPreferredWidth(120);
        tableChiTiet.getColumnModel().getColumn(1).setPreferredWidth(285);
        tableChiTiet.getColumnModel().getColumn(2).setPreferredWidth(80);
        tableChiTiet.getColumnModel().getColumn(3).setPreferredWidth(100);
        tableChiTiet.getColumnModel().getColumn(4).setPreferredWidth(100);
    }

    public void setTableData() {
        ArrayList<PhieuNhap> listPN = new ArrayList<>();
        PhieuNhap phieuNhap;
        resetTableData(tableListPhieuNhap);
        DefaultTableModel model = (DefaultTableModel) tableListPhieuNhap.getModel();
        switch (locTrangThai) {
            case "ALL":
                for (Map.Entry<String, PhieuNhap> entrySet : frMain.model.getListPhieuNhap().entrySet()) {
                    String key = entrySet.getKey();
                    phieuNhap = entrySet.getValue();
                    listPN.add(phieuNhap);
                }
                break;
            case "PHIEU_TAM":
                for (Map.Entry<String, PhieuNhap> entrySet : frMain.model.getListPhieuNhap().entrySet()) {
                    String key = entrySet.getKey();
                    phieuNhap = entrySet.getValue();
                    if (phieuNhap.getTrangThaiNhap().equalsIgnoreCase("phiếu tạm") == true) {
                        listPN.add(phieuNhap);
                    }
                }
                break;
            case "DA_NHAP":
                for (Map.Entry<String, PhieuNhap> entrySet : frMain.model.getListPhieuNhap().entrySet()) {
                    String key = entrySet.getKey();
                    phieuNhap = entrySet.getValue();
                    if (phieuNhap.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                        listPN.add(phieuNhap);
                    }
                }
                break;
            default:
                break;
        }
        switch (locThoiGian) {
            case "TOAN_THOI_GIAN":
                for (int i = 0; i < listPN.size(); i++) {
                    phieuNhap = listPN.get(i);
                    model.addRow(new Object[]{phieuNhap.getMa(), phieuNhap.getNgayLap(), phieuNhap.getTenNhaCungCap(), phieuNhap.getTrangThaiNhap()});
                }
                break;
            case "THANG_NAY":
                for (int i = 0; i < listPN.size(); i++) {
                    phieuNhap = listPN.get(i);
                    if (MyDate.getMonth(phieuNhap.getNgayLap()) == MyDate.getCurrentMonth()) {
                        model.addRow(new Object[]{phieuNhap.getMa(), phieuNhap.getNgayLap(), phieuNhap.getTenNhaCungCap(), phieuNhap.getTrangThaiNhap()});
                    }
                }
                break;
            case "THANG_TRUOC":
                for (int i = 0; i < listPN.size(); i++) {
                    phieuNhap = listPN.get(i);
                    if (MyDate.getMonth(phieuNhap.getNgayLap()) == MyDate.getPrevMonth()) {
                        if (MyDate.getPrevMonth() == 12) {
                            if (MyDate.getYear(phieuNhap.getNgayLap()) == (MyDate.getCurrentYear() - 1)) {
                                model.addRow(new Object[]{phieuNhap.getMa(), phieuNhap.getNgayLap(), phieuNhap.getTenNhaCungCap(), phieuNhap.getTrangThaiNhap()});
                            }
                        } else {
                            model.addRow(new Object[]{phieuNhap.getMa(), phieuNhap.getNgayLap(), phieuNhap.getTenNhaCungCap(), phieuNhap.getTrangThaiNhap()});
                        }
                    }
                }
                break;
            case "LUA_CHON_KHAC":
                for (int i = 0; i < listPN.size(); i++) {
                    phieuNhap = listPN.get(i);
                    Date date = MyDate.getDateFromString(phieuNhap.getNgayLap());
                    if (MyDate.compareDate(date1, date) <= 0 && MyDate.compareDate(date2, date) >= 0) {
                        model.addRow(new Object[]{phieuNhap.getMa(), phieuNhap.getNgayLap(), phieuNhap.getTenNhaCungCap(), phieuNhap.getTrangThaiNhap()});
                    }
                }
                break;
            default:
                break;
        }

    }

    public void setInfoChiTietPhieuNhap(PhieuNhap phieuNhap) {
        if (phieuNhap != null) {
            this.lbMaPN.setText(phieuNhap.getMa());
            this.lbTrangThai.setText(phieuNhap.getTrangThaiNhap());
            this.tfNgayNhap.setText(phieuNhap.getNgayLap());
            this.tfNhaCungCap.setText(phieuNhap.getTenNhaCungCap());

            // Thông tin chi tiết
            resetTableData(tableChiTiet);
            DefaultTableModel model = (DefaultTableModel) tableChiTiet.getModel();
            int tongTien = 0;
            int tongSoLuongSP = 0;
            for (int i = 0; i < phieuNhap.getListChiTiet().size(); i++) {
                String maSP = phieuNhap.getListChiTiet().get(i).getMaSP();
                String tenSP = "";
                int donGia = phieuNhap.getListChiTiet().get(i).getDonGia();
                int soLuong = 0;
                int thanhTien = 0;
                Object obj = frMain.model.getByKey(maSP);
                if (obj != null) {
                    BaseProduct pro = (BaseProduct) obj;
                    tenSP = pro.getTenSP();
                    soLuong = phieuNhap.getListChiTiet().get(i).getSoLuong();
                    thanhTien = soLuong * donGia;
                    model.addRow(new Object[]{maSP, tenSP, donGia, soLuong, thanhTien});
                }
                tongTien += thanhTien;
                tongSoLuongSP += soLuong;
            }
            this.lbTongSP.setText(tongSoLuongSP + "");
            this.lbTongTien.setText(tongTien + "");
        }
    }

    public void resetTableData(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void resetInfoChiTietPhieuNhap() {
        lbMaPN.setText("");
        lbTrangThai.setText("");
        lbTongTien.setText("0");
        lbTongSP.setText("0");
        tfNgayNhap.setText("");
        tfNhaCungCap.setText("");
        resetTableData(tableChiTiet);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupLocTrangThai = new javax.swing.ButtonGroup();
        btnGroupLocThoiGian = new javax.swing.ButtonGroup();
        panelLoc = new javax.swing.JPanel();
        panelNhomHang = new javax.swing.JPanel();
        labelTitle = new javax.swing.JLabel();
        tfSearch = new javax.swing.JTextField();
        panelNhomHang3 = new javax.swing.JPanel();
        labelTitle3 = new javax.swing.JLabel();
        radioPhieuTam = new javax.swing.JRadioButton();
        radioTatCa = new javax.swing.JRadioButton();
        radioDaNhap = new javax.swing.JRadioButton();
        panelNhomHang1 = new javax.swing.JPanel();
        labelTitle1 = new javax.swing.JLabel();
        radioThangNay = new javax.swing.JRadioButton();
        radioToanThoiGian = new javax.swing.JRadioButton();
        radioThangTruoc = new javax.swing.JRadioButton();
        radioLuaChonKhac = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        panelSanPham = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableListPhieuNhap = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        panelMain = new javax.swing.JPanel();
        panelBaseInfo = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lbTrangThai = new javax.swing.JLabel();
        lbMaPN = new javax.swing.JLabel();
        tfNhaCungCap = new javax.swing.JTextField();
        tfNgayNhap = new javax.swing.JTextField();
        btHuyBo = new javax.swing.JButton();
        btNhapHang = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableChiTiet = new javax.swing.JTable();
        labelNhan = new javax.swing.JLabel();
        lbTongSP = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lbTongTien = new javax.swing.JLabel();

        labelTitle.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle.setText("   Tìm kiếm");
        labelTitle.setAlignmentX(1.0F);
        labelTitle.setOpaque(true);

        tfSearch.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tfSearch.setText("Tìm theo mã phiếu nhập");
        tfSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfSearchMouseClicked(evt);
            }
        });
        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfSearchKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHangLayout = new javax.swing.GroupLayout(panelNhomHang);
        panelNhomHang.setLayout(panelNhomHangLayout);
        panelNhomHangLayout.setHorizontalGroup(
            panelNhomHangLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHangLayout.createSequentialGroup()
                .addGroup(panelNhomHangLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        panelNhomHangLayout.setVerticalGroup(
            panelNhomHangLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHangLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        labelTitle3.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle3.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle3.setText("   Lọc theo trạng thái");
        labelTitle3.setAlignmentX(1.0F);
        labelTitle3.setOpaque(true);

        btnGroupLocTrangThai.add(radioPhieuTam);
        radioPhieuTam.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioPhieuTam.setText("Phiếu tạm");
        radioPhieuTam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocTrangThai(evt);
            }
        });

        btnGroupLocTrangThai.add(radioTatCa);
        radioTatCa.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioTatCa.setSelected(true);
        radioTatCa.setText("Tất cả");
        radioTatCa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocTrangThai(evt);
            }
        });

        btnGroupLocTrangThai.add(radioDaNhap);
        radioDaNhap.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioDaNhap.setText("Đã nhập");
        radioDaNhap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocTrangThai(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang3Layout = new javax.swing.GroupLayout(panelNhomHang3);
        panelNhomHang3.setLayout(panelNhomHang3Layout);
        panelNhomHang3Layout.setHorizontalGroup(
            panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang3Layout.createSequentialGroup()
                .addComponent(labelTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNhomHang3Layout.createSequentialGroup()
                .addGap(0, 19, Short.MAX_VALUE)
                .addGroup(panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioTatCa, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioDaNhap, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioPhieuTam, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );
        panelNhomHang3Layout.setVerticalGroup(
            panelNhomHang3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang3Layout.createSequentialGroup()
                .addComponent(labelTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(radioTatCa, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioPhieuTam, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioDaNhap, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(243, 243, 243))
        );

        labelTitle1.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle1.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle1.setText("   Lọc thời gian");
        labelTitle1.setAlignmentX(1.0F);
        labelTitle1.setOpaque(true);

        btnGroupLocThoiGian.add(radioThangNay);
        radioThangNay.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioThangNay.setText("Tháng này");
        radioThangNay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        btnGroupLocThoiGian.add(radioToanThoiGian);
        radioToanThoiGian.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioToanThoiGian.setSelected(true);
        radioToanThoiGian.setText("Toàn thời gian");
        radioToanThoiGian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        btnGroupLocThoiGian.add(radioThangTruoc);
        radioThangTruoc.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioThangTruoc.setText("Tháng trước");
        radioThangTruoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        btnGroupLocThoiGian.add(radioLuaChonKhac);
        radioLuaChonKhac.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioLuaChonKhac.setText("Lựa chọn khác");
        radioLuaChonKhac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel1.setText("Từ ngày:");

        jDateChooser1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateChange(evt);
            }
        });

        jDateChooser2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateChange(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel3.setText("Đến ngày:");

        javax.swing.GroupLayout panelNhomHang1Layout = new javax.swing.GroupLayout(panelNhomHang1);
        panelNhomHang1.setLayout(panelNhomHang1Layout);
        panelNhomHang1Layout.setHorizontalGroup(
            panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang1Layout.createSequentialGroup()
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(radioThangTruoc, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioToanThoiGian, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioThangNay, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelNhomHang1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelNhomHang1Layout.createSequentialGroup()
                                .addComponent(radioLuaChonKhac, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNhomHang1Layout.createSequentialGroup()
                                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addGap(23, 23, 23))
        );
        panelNhomHang1Layout.setVerticalGroup(
            panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang1Layout.createSequentialGroup()
                .addComponent(labelTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioToanThoiGian, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioThangNay, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioThangTruoc, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioLuaChonKhac, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout panelLocLayout = new javax.swing.GroupLayout(panelLoc);
        panelLoc.setLayout(panelLocLayout);
        panelLocLayout.setHorizontalGroup(
            panelLocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLocLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelNhomHang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
            .addGroup(panelLocLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelNhomHang3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelNhomHang1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelLocLayout.setVerticalGroup(
            panelLocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocLayout.createSequentialGroup()
                .addComponent(panelNhomHang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelNhomHang3, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelNhomHang1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(12, 12, 12))
        );

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        tableListPhieuNhap.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        tableListPhieuNhap.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã phiếu nhập", "Ngày nhập", "Nhà cung cấp", "Trạng thái nhập"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tableListPhieuNhap.setToolTipText("");
        tableListPhieuNhap.setAlignmentX(1.0F);
        tableListPhieuNhap.setRowHeight(25);
        tableListPhieuNhap.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableListPhieuNhapMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableListPhieuNhap);

        jButton2.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/plus_icon.png"))); // NOI18N
        jButton2.setText("Thêm Phiếu Nhập");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        panelMain.setBackground(new java.awt.Color(255, 255, 255));

        panelBaseInfo.setBackground(new java.awt.Color(255, 255, 255));
        panelBaseInfo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel4.setText("Ngày nhập");

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel5.setText("Mã phiếu nhập:");

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel6.setText("Trạng thái");

        jLabel8.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel8.setText("Nhà cung cấp");

        lbTrangThai.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        lbMaPN.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        tfNhaCungCap.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tfNhaCungCap.setEnabled(false);

        tfNgayNhap.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tfNgayNhap.setEnabled(false);

        javax.swing.GroupLayout panelBaseInfoLayout = new javax.swing.GroupLayout(panelBaseInfo);
        panelBaseInfo.setLayout(panelBaseInfoLayout);
        panelBaseInfoLayout.setHorizontalGroup(
            panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfoLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbMaPN, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbTrangThai, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfNhaCungCap, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                    .addComponent(tfNgayNhap))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelBaseInfoLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(194, Short.MAX_VALUE)))
        );
        panelBaseInfoLayout.setVerticalGroup(
            panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfoLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(lbMaPN, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBaseInfoLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(panelBaseInfoLayout.createSequentialGroup()
                        .addComponent(lbTrangThai, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)))
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfNgayNhap, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfNhaCungCap, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addContainerGap(27, Short.MAX_VALUE))
            .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelBaseInfoLayout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(139, Short.MAX_VALUE)))
        );

        btHuyBo.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btHuyBo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Delete.png"))); // NOI18N
        btHuyBo.setText("Hủy bỏ");
        btHuyBo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHuyBoActionPerformed(evt);
            }
        });

        btNhapHang.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btNhapHang.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/import.gif"))); // NOI18N
        btNhapHang.setText("Nhập hàng");
        btNhapHang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNhapHangActionPerformed(evt);
            }
        });

        tableChiTiet.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tableChiTiet.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã sản phẩm", "Tên sản phẩm", "Đơn giá", "Số lượng", "Thành tiền"
            }
        ));
        tableChiTiet.setRowHeight(25);
        jScrollPane2.setViewportView(tableChiTiet);

        labelNhan.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        labelNhan.setForeground(new java.awt.Color(0, 204, 204));
        labelNhan.setText("Tổng số sản phẩm:");

        lbTongSP.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongSP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongSP.setText("0");
        lbTongSP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 204)));

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 204, 204));
        jLabel2.setText("Tổng tiền hàng:");

        lbTongTien.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongTien.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTien.setText("0");
        lbTongTien.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 204)));

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(btHuyBo)
                        .addGap(18, 18, 18)
                        .addComponent(btNhapHang)
                        .addGap(71, 71, 71)
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelNhan, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbTongTien, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
                            .addComponent(lbTongSP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(panelBaseInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 696, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(panelBaseInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btHuyBo)
                    .addComponent(btNhapHang)
                    .addComponent(labelNhan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbTongSP, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout panelSanPhamLayout = new javax.swing.GroupLayout(panelSanPham);
        panelSanPham.setLayout(panelSanPhamLayout);
        panelSanPhamLayout.setHorizontalGroup(
            panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSanPhamLayout.createSequentialGroup()
                .addGroup(panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelSanPhamLayout.createSequentialGroup()
                        .addGroup(panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelSanPhamLayout.createSequentialGroup()
                                .addGap(820, 820, 820)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1049, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelSanPhamLayout.setVerticalGroup(
            panelSanPhamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSanPhamLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelLoc, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelSanPham, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelSanPham, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
            .addComponent(panelLoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableListPhieuNhapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableListPhieuNhapMouseClicked
        // TODO add your handling code here:
        String maPhieuNhap = tableListPhieuNhap.getValueAt(tableListPhieuNhap.getSelectedRow(), 0).toString();
        Object obj = frMain.model.getByKey(maPhieuNhap);
        if (obj != null && (obj instanceof PhieuNhap)) {
            setInfoChiTietPhieuNhap((PhieuNhap) obj);
        }
    }//GEN-LAST:event_tableListPhieuNhapMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.frMain.setFormPanelMain("ADD PHIẾU NHẬP");
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btHuyBoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHuyBoActionPerformed
        // TODO add your handling code here:
        String maPN = lbMaPN.getText();
        Object obj = frMain.model.getByKey(maPN);
        if (obj != null && (obj instanceof PhieuNhap)) {
            PhieuNhap phieuNhap = (PhieuNhap) obj;
            if (phieuNhap.getTrangThaiNhap().equalsIgnoreCase("phiếu tạm") == true) {
                int choose = JOptionPane.showConfirmDialog(null, "Bạn có chắc muốn hủy phiếu nhập này không?", "Hủy phiếu nhập", JOptionPane.YES_NO_CANCEL_OPTION);
                if (choose == JOptionPane.YES_OPTION) {
                    frMain.model.delByKey(maPN);
                    JOptionPane.showMessageDialog(null, "Hủy bỏ thành công!", "Thông báo", JOptionPane.INFORMATION_MESSAGE);
                    // cập nhật lại bảng danh sách phiếu nhập & phần thông tin chi tiết phiếu nhập
                    frMain.UpdateAll();
                    resetInfoChiTietPhieuNhap();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Lỗi! Không thể hủy phiếu đã nhập!\nChỉ có thể hủy phiếu tạm", "Thông baó", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btHuyBoActionPerformed

    private void btNhapHangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNhapHangActionPerformed
        // TODO add your handling code here:
        String maPN = lbMaPN.getText();
        Object obj = frMain.model.getByKey(maPN);
        if (obj != null && (obj instanceof PhieuNhap)) {
            PhieuNhap phieuNhap = (PhieuNhap) obj;
            if (phieuNhap.getTrangThaiNhap().equalsIgnoreCase("phiếu tạm") == true) {
                phieuNhap.setTrangThaiNhap("Đã nhập");
                frMain.model.update(phieuNhap);
                JOptionPane.showMessageDialog(null, "Nhập hàng thành công!", "Thông baó", JOptionPane.INFORMATION_MESSAGE);
                this.lbTrangThai.setText("Đã nhập");
                frMain.UpdateAll();
            } else {
                JOptionPane.showMessageDialog(null, "Lỗi! Phiếu này đã nhập rồi!\nChỉ có thể nhập hàng với phiếu tạm", "Thông baó", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btNhapHangActionPerformed

    private void LocTrangThai(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocTrangThai
        // TODO add your handling code here:
        if (radioTatCa.isSelected()) {
            locTrangThai = "ALL";
        } else if (radioPhieuTam.isSelected()) {
            locTrangThai = "PHIEU_TAM";
        } else if (radioDaNhap.isSelected()) {
            locTrangThai = "DA_NHAP";
        }
        setTableData();
    }//GEN-LAST:event_LocTrangThai

    private void LocThoiGian(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocThoiGian
        // TODO add your handling code here:
//        if (radioToanThoiGian.isSelected()) {
//            locThoiGian = "TOAN_THOI_GIAN";
//        } else if (radioThangNay.isSelected()) {
//            locThoiGian = "THANG_NAY";
//        } else if (radioThangTruoc.isSelected()) {
//            locThoiGian = "THANG_TRUOC";
//        } else if (radioLuaChonKhac.isSelected()) {
//            locThoiGian = "LUA_CHON_KHAC";
//        }
//        setTableData();
        LocThoiGian();
    }//GEN-LAST:event_LocThoiGian

    private void DateChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_DateChange
        // TODO add your handling code here:
        date1 = jDateChooser1.getDate();
        date2 = jDateChooser2.getDate();
        if (locThoiGian.equals("LUA_CHON_KHAC") == true) {
            setTableData();
        }
    }//GEN-LAST:event_DateChange

    private void tfSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//            DefaultTableModel model = (DefaultTableModel) tableListPhieuNhap.getModel();
//            for(int i = (model.getRowCount() - 1); i >= 0; i--){
//                String maPN = tableListPhieuNhap.getValueAt(i, 0).toString();
//                if(maPN.equalsIgnoreCase(tfSearch.getText()) == false)
//                    model.removeRow(i);
//            }
            TimKiem(tfSearch.getText());
        }
    }//GEN-LAST:event_tfSearchKeyPressed

    private void tfSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfSearchMouseClicked
        // TODO add your handling code here:
        tfSearch.selectAll();
    }//GEN-LAST:event_tfSearchMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btHuyBo;
    private javax.swing.JButton btNhapHang;
    private javax.swing.ButtonGroup btnGroupLocThoiGian;
    private javax.swing.ButtonGroup btnGroupLocTrangThai;
    private javax.swing.JButton jButton2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel labelNhan;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JLabel labelTitle1;
    private javax.swing.JLabel labelTitle3;
    private javax.swing.JLabel lbMaPN;
    private javax.swing.JLabel lbTongSP;
    private javax.swing.JLabel lbTongTien;
    private javax.swing.JLabel lbTrangThai;
    private javax.swing.JPanel panelBaseInfo;
    private javax.swing.JPanel panelLoc;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelNhomHang;
    private javax.swing.JPanel panelNhomHang1;
    private javax.swing.JPanel panelNhomHang3;
    private javax.swing.JPanel panelSanPham;
    private javax.swing.JRadioButton radioDaNhap;
    private javax.swing.JRadioButton radioLuaChonKhac;
    private javax.swing.JRadioButton radioPhieuTam;
    private javax.swing.JRadioButton radioTatCa;
    private javax.swing.JRadioButton radioThangNay;
    private javax.swing.JRadioButton radioThangTruoc;
    private javax.swing.JRadioButton radioToanThoiGian;
    private javax.swing.JTable tableChiTiet;
    private javax.swing.JTable tableListPhieuNhap;
    private javax.swing.JTextField tfNgayNhap;
    private javax.swing.JTextField tfNhaCungCap;
    private javax.swing.JTextField tfSearch;
    // End of variables declaration//GEN-END:variables

    @Override
    public void TimKiem(String textSearch) {
        DefaultTableModel model = (DefaultTableModel) tableListPhieuNhap.getModel();
        for (int i = (model.getRowCount() - 1); i >= 0; i--) {
            String maPN = tableListPhieuNhap.getValueAt(i, 0).toString();
            if (maPN.equalsIgnoreCase(textSearch) == false) {
                model.removeRow(i);
            }
        }
    }

    @Override
    public void LocThoiGian() {
        if (radioToanThoiGian.isSelected()) {
            locThoiGian = "TOAN_THOI_GIAN";
        } else if (radioThangNay.isSelected()) {
            locThoiGian = "THANG_NAY";
        } else if (radioThangTruoc.isSelected()) {
            locThoiGian = "THANG_TRUOC";
        } else if (radioLuaChonKhac.isSelected()) {
            locThoiGian = "LUA_CHON_KHAC";
        }
        setTableData();
    }
}
