/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import Helper.MyDate;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import product.ChiPhiPhatSinh;
import product.HoaDon;
import product.PhieuNhap;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class FormBaoCao extends javax.swing.JInternalFrame implements LocThoiGian {

    /**
     * Creates new form FormBaoCao
     */
    Date date1;
    Date date2;
    String locThoiGian = "THANG_NAY";
    FormMain frMain;

    public FormBaoCao(FormMain frMain) {
        initComponents();
        this.setLocation(-5, -27);
        jDateChooser1.setDate(new Date());
        jDateChooser2.setDate(new Date());
        this.frMain = frMain;
        baoCaoNhapHang();
        baoCaoBanHang();
        baocaoDoanhThu();
    }

    public void baoCaoNhapHang() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int tongTien = 0;
        switch (locThoiGian) {
            case "THANG_NAY":
                int curMonth = MyDate.getCurrentMonth();
                int year = MyDate.getCurrentYear();
                ArrayList<PhieuNhap> arr = frMain.model.getPhieuNhapInMonth(curMonth, year);
                ArrayList<Date> listDate = sort(getListDate(arr));
                for (Date date : listDate) {
                    Date date1 = date;
                    Date date2 = date;
                    ArrayList<PhieuNhap> listPNinDay = frMain.model.getPhieuNhapByDate(date1, date2);
                    for (int i = 0; i < listPNinDay.size(); i++) {
                        PhieuNhap pn = listPNinDay.get(i);
                        if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                            dataset.setValue(pn.tongTien(), pn.getMa(), pn.getNgayLap());
                            tongTien += pn.tongTien();
                        }
                    }
                }
                break;
            case "THANG_TRUOC":
                int prevMonth = MyDate.getPrevMonth();
                year = MyDate.getCurrentYear();
                if (prevMonth == 12) {
                    year -= 1;
                }
                arr = frMain.model.getPhieuNhapInMonth(prevMonth, year);
                listDate = sort(getListDate(arr));
                for (Date date : listDate) {
                    Date date1 = date;
                    Date date2 = date;
                    ArrayList<PhieuNhap> listPNinDay = frMain.model.getPhieuNhapByDate(date1, date2);
                    for (int i = 0; i < listPNinDay.size(); i++) {
                        PhieuNhap pn = listPNinDay.get(i);
                        if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                            dataset.setValue(pn.tongTien(), pn.getMa(), pn.getNgayLap());
                            tongTien += pn.tongTien();
                        }
                    }
                }
                break;
            case "LUA_CHON_KHAC":
                date1 = jDateChooser1.getDate();
                date2 = jDateChooser2.getDate();
                arr = frMain.model.getPhieuNhapByDate(date1, date2);
                listDate = sort(getListDate(arr));
                for (Date date : listDate) {
                    Date date1 = date;
                    Date date2 = date;
                    ArrayList<PhieuNhap> listPNinDay = frMain.model.getPhieuNhapByDate(date1, date2);
                    for (int i = 0; i < listPNinDay.size(); i++) {
                        PhieuNhap pn = listPNinDay.get(i);
                        if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                            dataset.setValue(pn.tongTien(), pn.getMa(), pn.getNgayLap());
                            tongTien += pn.tongTien();
                        }
                    }
                }
                break;
            default:
                break;
        }
        JFreeChart chart = ChartFactory.createBarChart("Biểu đồ nhập hàng", "Phiếu nhập", "Tổng tiền", dataset, PlotOrientation.VERTICAL, false, true, false);
        //JFreeChart chart = ChartFactory.createBarChart3D("Danh thu bán hàng", "Tên sản phẩm", "Doanh thu", dataset, PlotOrientation.VERTICAL, false, true, false);
        chart.setBackgroundPaint(Color.white);
        chart.getTitle().setPaint(Color.black);
        CategoryPlot p = chart.getCategoryPlot();
        p.setRangeGridlinePaint(Color.GREEN);

        ChartPanel charpn = new ChartPanel(chart);
        panelNhapHang.removeAll();
        panelNhapHang.add(charpn, BorderLayout.CENTER);
        panelNhapHang.validate();
        lbTongTienNhapHang.setText(tongTien + " VND");
    }

    public void baoCaoBanHang() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int tongTien = 0;
        switch (locThoiGian) {
            case "THANG_NAY":
                int curMonth = MyDate.getCurrentMonth();
                int year = MyDate.getCurrentYear();
                ArrayList<HoaDon> arr = frMain.model.getHoaDonInMonth(curMonth, year);
                ArrayList<Date> listDate = sort(getListDate1(arr));
                for (Date date : listDate) {
                    Date date1 = date;
                    Date date2 = date;
                    ArrayList<HoaDon> listHDinDay = frMain.model.getHoaDonByDate(date1, date2);
                    for (int i = 0; i < listHDinDay.size(); i++) {
                        HoaDon hd = listHDinDay.get(i);
                        dataset.setValue(hd.tongTien(), hd.getMa(), hd.getNgayLap());
                        tongTien += hd.tongTien();
                    }
                }
                break;
            case "THANG_TRUOC":
                int prevMonth = MyDate.getPrevMonth();
                year = MyDate.getCurrentYear();
                if (prevMonth == 12) {
                    year -= 1;
                }
                arr = frMain.model.getHoaDonInMonth(prevMonth, year);
                listDate = sort(getListDate1(arr));
                for (Date date : listDate) {
                    Date date1 = date;
                    Date date2 = date;
                    ArrayList<HoaDon> listHDinDay = frMain.model.getHoaDonByDate(date1, date2);
                    for (int i = 0; i < listHDinDay.size(); i++) {
                        HoaDon hd = listHDinDay.get(i);
                        dataset.setValue(hd.tongTien(), hd.getMa(), hd.getNgayLap());
                        tongTien += hd.tongTien();
                    }
                }
                break;
            case "LUA_CHON_KHAC":
                date1 = jDateChooser1.getDate();
                date2 = jDateChooser2.getDate();
                arr = frMain.model.getHoaDonByDate(date1, date2);
                listDate = sort(getListDate1(arr));
                for (Date date : listDate) {
                    Date date1 = date;
                    Date date2 = date;
                    ArrayList<HoaDon> listHDinDay = frMain.model.getHoaDonByDate(date1, date2);
                    for (int i = 0; i < listHDinDay.size(); i++) {
                        HoaDon hd = listHDinDay.get(i);
                        dataset.setValue(hd.tongTien(), hd.getMa(), hd.getNgayLap());
                        tongTien += hd.tongTien();
                    }
                }
                break;

            default:
                break;
        }
        JFreeChart chart = ChartFactory.createBarChart("Biểu đồ bán hàng", "Hóa Đơn", "Tổng tiền", dataset, PlotOrientation.VERTICAL, false, true, false);
        //JFreeChart chart = ChartFactory.createBarChart3D("Danh thu bán hàng", "Tên sản phẩm", "Doanh thu", dataset, PlotOrientation.VERTICAL, false, true, false);
        chart.setBackgroundPaint(Color.white);
        chart.getTitle().setPaint(Color.black);
        CategoryPlot p = chart.getCategoryPlot();
        p.setRangeGridlinePaint(Color.GREEN);

        ChartPanel charpn = new ChartPanel(chart);
        panelBanHang.removeAll();
        panelBanHang.add(charpn, BorderLayout.CENTER);
        panelBanHang.validate();
        lbTongTien.setText(tongTien + " VND");
    }

    public void baocaoDoanhThu() {
        DefaultPieDataset pieData = new DefaultPieDataset();
        int tongTienNhapHang = 0;
        int tongTienPhatSinh = 0;
        int tongTienBanHang = 0;
        switch (locThoiGian) {
            case "THANG_NAY":
                int curMonth = MyDate.getCurrentMonth();
                int year = MyDate.getCurrentYear();
                ArrayList<PhieuNhap> arr = frMain.model.getPhieuNhapInMonth(curMonth, year);
                for (PhieuNhap pn : arr) {
                    if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                        tongTienNhapHang += pn.tongTien();
                    }
                }
                ArrayList<HoaDon> arr1 = frMain.model.getHoaDonInMonth(curMonth, year);
                for (HoaDon hd : arr1) {
                    tongTienBanHang += hd.tongTien();
                }
                ArrayList<ChiPhiPhatSinh> arr2 = frMain.model.getCppsInMonth(curMonth, year);
                for (ChiPhiPhatSinh cpps : arr2) {
                    tongTienPhatSinh += cpps.getTongTien();
                }

                break;
            case "THANG_TRUOC":
                int prevMonth = MyDate.getPrevMonth();
                year = MyDate.getCurrentYear();
                if (prevMonth == 12) {
                    year -= 1;
                }
                arr = frMain.model.getPhieuNhapInMonth(prevMonth, year);
                for (PhieuNhap pn : arr) {
                    if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                        tongTienNhapHang += pn.tongTien();
                    }
                }
                arr1 = frMain.model.getHoaDonInMonth(prevMonth, year);
                for (HoaDon hd : arr1) {
                    tongTienBanHang += hd.tongTien();
                }
                arr2 = frMain.model.getCppsInMonth(prevMonth, year);
                for (ChiPhiPhatSinh cpps : arr2) {
                    tongTienPhatSinh += cpps.getTongTien();
                }
                break;
            case "LUA_CHON_KHAC":
                date1 = jDateChooser1.getDate();
                date2 = jDateChooser2.getDate();
                arr = frMain.model.getPhieuNhapByDate(date1, date2);
                for (PhieuNhap pn : arr) {
                    if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
                        tongTienNhapHang += pn.tongTien();
                    }
                }
                arr1 = frMain.model.getHoaDonByDate(date1, date2);
                for (HoaDon hd : arr1) {
                    tongTienBanHang += hd.tongTien();
                }
                arr2 = frMain.model.getCppsByDate(date1, date2);
                for (ChiPhiPhatSinh cpps : arr2) {
                    tongTienPhatSinh += cpps.getTongTien();
                }
                break;
            default:
                break;
        }
        pieData.setValue("Nhập hàng", tongTienNhapHang);
        pieData.setValue("Bán hàng", tongTienBanHang);
        pieData.setValue("Chi Phí phát sinh", tongTienPhatSinh);
        JFreeChart chart = ChartFactory.createPieChart("Biểu đồ doanh thu", pieData, true, true, false);
        //JFreeChart chart = ChartFactory.createPieChart3D("Biểu đồ doanh thu", pieData,true, true, false);
        PiePlot p = (PiePlot) chart.getPlot();
        //p.setForegroundAlpha(TOP_ALIGNMENT);
        ChartPanel pn = new ChartPanel(chart);
        panelBieuDo.removeAll();
        panelBieuDo.add(pn, BorderLayout.CENTER);
        panelBieuDo.validate();

        lbTongTienNhap.setText(tongTienNhapHang + " VND");
        lbTongTienBan.setText(tongTienBanHang + " VND");
        lbTongTienCPPS.setText(tongTienPhatSinh + " VND");
        int lai = (tongTienBanHang - tongTienNhapHang - tongTienPhatSinh);
        if (lai >= 0) {
            lbTitleLaiLo.setText("Tổng lãi:");
        } else {
            lbTitleLaiLo.setText("Tổng lỗ:");
        }
        lbLaiLo.setText(lai + " VND");
    }

    public HashMap<Date, Date> getListDate(ArrayList<PhieuNhap> arr) {
        HashMap<Date, Date> hm = new HashMap<>();
        for (PhieuNhap pn : arr) {
            Date date = MyDate.getDateFromString(pn.getNgayLap());
            if (hm.containsKey(date) == false) {
                hm.put(date, date);
            }
        }
        return hm;
    }

    public HashMap<Date, Date> getListDate1(ArrayList<HoaDon> arr) {
        HashMap<Date, Date> hm = new HashMap<>();
        for (HoaDon hd : arr) {
            Date date = MyDate.getDateFromString(hd.getNgayLap());
            if (hm.containsKey(date) == false) {
                hm.put(date, date);
            }
        }
        return hm;
    }

    public ArrayList<Date> sort(HashMap<Date, Date> hm) {
        ArrayList<Date> arr = new ArrayList<>();
        for (Map.Entry<Date, Date> entrySet : hm.entrySet()) {
            Date date = entrySet.getValue();
            arr.add(date);
        }
        Collections.sort(arr, new DateComparator());
        return arr;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelNhomHang1 = new javax.swing.JPanel();
        labelTitle1 = new javax.swing.JLabel();
        radioThangNay = new javax.swing.JRadioButton();
        radioThangTruoc = new javax.swing.JRadioButton();
        radioLuaChonKhac = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        panelNhapHang = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        lbTongTienNhapHang = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        panelBanHang = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lbTongTien = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        panelBieuDo = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lbTongTienNhap = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbTongTienBan = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lbTongTienCPPS = new javax.swing.JLabel();
        lbTitleLaiLo = new javax.swing.JLabel();
        lbLaiLo = new javax.swing.JLabel();

        labelTitle1.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle1.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle1.setText("   Lọc thời gian");
        labelTitle1.setAlignmentX(1.0F);
        labelTitle1.setOpaque(true);

        buttonGroup1.add(radioThangNay);
        radioThangNay.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioThangNay.setSelected(true);
        radioThangNay.setText("Tháng này");
        radioThangNay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        buttonGroup1.add(radioThangTruoc);
        radioThangTruoc.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioThangTruoc.setText("Tháng trước");
        radioThangTruoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        buttonGroup1.add(radioLuaChonKhac);
        radioLuaChonKhac.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioLuaChonKhac.setText("Lựa chọn khác");
        radioLuaChonKhac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocThoiGian(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel1.setText("Từ ngày:");

        jDateChooser1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateChange(evt);
            }
        });

        jDateChooser2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateChange(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel3.setText("Đến ngày:");

        javax.swing.GroupLayout panelNhomHang1Layout = new javax.swing.GroupLayout(panelNhomHang1);
        panelNhomHang1.setLayout(panelNhomHang1Layout);
        panelNhomHang1Layout.setHorizontalGroup(
            panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNhomHang1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(labelTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelNhomHang1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioThangTruoc, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioLuaChonKhac, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioThangNay, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelNhomHang1Layout.setVerticalGroup(
            panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang1Layout.createSequentialGroup()
                .addComponent(labelTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioThangNay, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioThangTruoc, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioLuaChonKhac, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNhomHang1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N

        panelNhapHang.setBackground(new java.awt.Color(255, 255, 255));
        panelNhapHang.setLayout(new java.awt.BorderLayout());

        jLabel4.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 51, 102));
        jLabel4.setText("Tổng tiền nhập hàng");

        lbTongTienNhapHang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbTongTienNhapHang.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTienNhapHang.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelNhapHang, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbTongTienNhapHang, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 657, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(lbTongTienNhapHang, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(panelNhapHang, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(220, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("NHẬP HÀNG", jPanel2);

        panelBanHang.setBackground(new java.awt.Color(255, 255, 255));
        panelBanHang.setLayout(new java.awt.BorderLayout());

        jLabel2.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 51, 102));
        jLabel2.setText("Tổng tiền bán hàng");

        lbTongTien.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbTongTien.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTien.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelBanHang, javax.swing.GroupLayout.PREFERRED_SIZE, 1063, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(lbTongTien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelBanHang, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(214, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("BÁN HÀNG", jPanel1);

        panelBieuDo.setBackground(new java.awt.Color(255, 255, 255));
        panelBieuDo.setLayout(new java.awt.BorderLayout());

        jPanel4.setBackground(new java.awt.Color(204, 204, 255));

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 102));
        jLabel5.setText("Tổng tiền nhập hàng: ");

        lbTongTienNhap.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongTienNhap.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTienNhap.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        jLabel7.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 102));
        jLabel7.setText("Tổng tiền bán hàng: ");

        lbTongTienBan.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongTienBan.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTienBan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        jLabel13.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 0, 102));
        jLabel13.setText("Tổng tiền chi phí phát sinh: ");

        lbTongTienCPPS.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongTienCPPS.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTienCPPS.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        lbTitleLaiLo.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        lbTitleLaiLo.setForeground(new java.awt.Color(255, 0, 51));
        lbTitleLaiLo.setText("Lãi or Lỗ");

        lbLaiLo.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbLaiLo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbLaiLo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbTitleLaiLo, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE))
                        .addGap(18, 18, 18)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbTongTienNhap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbTongTienBan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbTongTienCPPS, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(lbLaiLo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(65, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTongTienNhap, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTongTienBan, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTongTienCPPS, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTitleLaiLo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLaiLo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(75, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(panelBieuDo, javax.swing.GroupLayout.PREFERRED_SIZE, 519, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelBieuDo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(278, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("DOANH THU", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelNhomHang1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1089, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 766, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelNhomHang1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LocThoiGian(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocThoiGian
        // TODO add your handling code here:
        LocThoiGian();
    }//GEN-LAST:event_LocThoiGian

    private void DateChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_DateChange
        // TODO add your handling code here:
        date1 = jDateChooser1.getDate();
        date2 = jDateChooser2.getDate();
        if (radioLuaChonKhac.isSelected()) {
            locThoiGian = "LUA_CHON_KHAC";
            baoCaoNhapHang();
            baoCaoBanHang();
            baocaoDoanhThu();
        }
    }//GEN-LAST:event_DateChange


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel labelTitle1;
    private javax.swing.JLabel lbLaiLo;
    private javax.swing.JLabel lbTitleLaiLo;
    private javax.swing.JLabel lbTongTien;
    private javax.swing.JLabel lbTongTienBan;
    private javax.swing.JLabel lbTongTienCPPS;
    private javax.swing.JLabel lbTongTienNhap;
    private javax.swing.JLabel lbTongTienNhapHang;
    private javax.swing.JPanel panelBanHang;
    private javax.swing.JPanel panelBieuDo;
    private javax.swing.JPanel panelNhapHang;
    private javax.swing.JPanel panelNhomHang1;
    private javax.swing.JRadioButton radioLuaChonKhac;
    private javax.swing.JRadioButton radioThangNay;
    private javax.swing.JRadioButton radioThangTruoc;
    // End of variables declaration//GEN-END:variables

    @Override
    public void LocThoiGian() {
        if (radioThangNay.isSelected()) {
            locThoiGian = "THANG_NAY";
        } else if (radioThangTruoc.isSelected()) {
            locThoiGian = "THANG_TRUOC";
        } else if (radioLuaChonKhac.isSelected()) {
            locThoiGian = "LUA_CHON_KHAC";
        }
        baoCaoNhapHang();
        baoCaoBanHang();
        baocaoDoanhThu();
    }
}

class DateComparator implements Comparator<Date> {

    @Override
    public int compare(Date date1, Date date2) {
        return MyDate.compareDate(date1, date2);
    }

}
