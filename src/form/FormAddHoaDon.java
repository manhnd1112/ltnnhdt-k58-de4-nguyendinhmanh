/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import Config.Config;
import image.ImageProcess;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import product.BaseProduct;
import product.ChiTiet;
import product.DiaNhac;
import product.DiaPhim;
import product.HoaDon;
import product.Sach;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class FormAddHoaDon extends javax.swing.JInternalFrame implements TimKiem,LocNhomSanPham{

    /**
     * Creates new form FormAddHoaDon
     */
    FormMain frMain;
    public int tongSP;
    public int tongTien;
    public String nhomSP = "ALL";

    public FormAddHoaDon(FormMain frMain) {
        try {
            initComponents();
            this.setLocation(-5, -27);
            this.setVisible(true);
            this.frMain = frMain;

            UIManager.setLookAndFeel(Config.windowsClassName);
            SwingUtilities.updateComponentTreeUI(this);
            Date date = new Date();
            jDateChooser1.setDate(date);
            setViewTable();
            setTableData();
        } catch (Exception ex) {
            Logger.getLogger(FormAddHoaDon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setViewTable() {
        Font font = new Font("sans-serif", Font.BOLD, 14);
        tableListSP.getTableHeader().setFont(font);
        tableListSP.getTableHeader().setForeground(Color.red);
        tableListSP.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableListSP.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableListSP.getColumnModel().getColumn(1).setPreferredWidth(350);
        tableListSP.getColumnModel().getColumn(2).setPreferredWidth(150);
        tableListSP.getColumnModel().getColumn(3).setPreferredWidth(110);

        Font font1 = new Font("sans-serif", Font.BOLD, 14);
        tableChiTiet.getTableHeader().setFont(font1);
        tableChiTiet.getTableHeader().setForeground(Color.blue);
        tableChiTiet.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableChiTiet.getColumnModel().getColumn(0).setPreferredWidth(100);
        tableChiTiet.getColumnModel().getColumn(1).setPreferredWidth(240);
        tableChiTiet.getColumnModel().getColumn(2).setPreferredWidth(85);
        tableChiTiet.getColumnModel().getColumn(3).setPreferredWidth(80);
        tableChiTiet.getColumnModel().getColumn(4).setPreferredWidth(100);
    }

    public void setTableData() {
        ArrayList<BaseProduct> listSP = new ArrayList<>();
        BaseProduct pro;
        resetTableData(tableListSP);
        DefaultTableModel model = (DefaultTableModel) tableListSP.getModel();
        switch (nhomSP) {
            case "ALL":
                for (Map.Entry<String, BaseProduct> entrySet : frMain.model.getListPro().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                }
                break;
            case "NHAC":
                for (Map.Entry<String, DiaNhac> entrySet : frMain.model.getListDiaNhac().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                }
                break;
            case "PHIM":
                for (Map.Entry<String, DiaPhim> entrySet : frMain.model.getListDiaPhim().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                }
                break;
            case "SACH":
                for (Map.Entry<String, Sach> entrySet : frMain.model.getListSach().entrySet()) {
                    String key = entrySet.getKey();
                    pro = entrySet.getValue();
                    listSP.add(pro);
                }
                break;
            default:
                break;
        }
        for (int i = 0; i < listSP.size(); i++) {
            pro = listSP.get(i);
            model.addRow(new Object[]{pro.getMaSP(), pro.getTenSP(), pro.getGiaBan(), pro.getSoluong()});
        }
    }

    public void resetTableData(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelLoc = new javax.swing.JPanel();
        panelTimKiem = new javax.swing.JPanel();
        labelSearch = new javax.swing.JLabel();
        tfSearch = new javax.swing.JTextField();
        panelNhomHang2 = new javax.swing.JPanel();
        labelTitle2 = new javax.swing.JLabel();
        radioTatCa = new javax.swing.JRadioButton();
        radioDiaNhac = new javax.swing.JRadioButton();
        radioSach = new javax.swing.JRadioButton();
        radioDiaPhim = new javax.swing.JRadioButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        labelAvatar = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        spSoLuong = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        tfGiaBan = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        panelMain = new javax.swing.JPanel();
        panelBaseInfo = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        tfTenKhachHang = new javax.swing.JTextField();
        tfSDT = new javax.swing.JTextField();
        tfEmail = new javax.swing.JTextField();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        panelChiTiet = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableChiTiet = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        lbTongTien = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbTongSP = new javax.swing.JLabel();
        btSave = new javax.swing.JButton();
        btHuy = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableListSP = new javax.swing.JTable();

        labelSearch.setBackground(new java.awt.Color(0, 153, 204));
        labelSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelSearch.setForeground(new java.awt.Color(255, 255, 255));
        labelSearch.setText("   Tìm kiếm");
        labelSearch.setAlignmentX(1.0F);
        labelSearch.setOpaque(true);

        tfSearch.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tfSearch.setText("Tìm theo tên, hoặc mã sản phẩm");
        tfSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfSearchMouseClicked(evt);
            }
        });
        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfSearchKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelTimKiemLayout = new javax.swing.GroupLayout(panelTimKiem);
        panelTimKiem.setLayout(panelTimKiemLayout);
        panelTimKiemLayout.setHorizontalGroup(
            panelTimKiemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTimKiemLayout.createSequentialGroup()
                .addGroup(panelTimKiemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelTimKiemLayout.setVerticalGroup(
            panelTimKiemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTimKiemLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelTitle2.setBackground(new java.awt.Color(0, 153, 204));
        labelTitle2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTitle2.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle2.setText("   Nhóm hàng");
        labelTitle2.setAlignmentX(1.0F);
        labelTitle2.setOpaque(true);

        buttonGroup1.add(radioTatCa);
        radioTatCa.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioTatCa.setSelected(true);
        radioTatCa.setText("Tất cả");
        radioTatCa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        buttonGroup1.add(radioDiaNhac);
        radioDiaNhac.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioDiaNhac.setText("Đĩa nhạc");
        radioDiaNhac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        buttonGroup1.add(radioSach);
        radioSach.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioSach.setText("Sách");
        radioSach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        buttonGroup1.add(radioDiaPhim);
        radioDiaPhim.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        radioDiaPhim.setText("Đĩa phim");
        radioDiaPhim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LocNhomSP(evt);
            }
        });

        javax.swing.GroupLayout panelNhomHang2Layout = new javax.swing.GroupLayout(panelNhomHang2);
        panelNhomHang2.setLayout(panelNhomHang2Layout);
        panelNhomHang2Layout.setHorizontalGroup(
            panelNhomHang2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang2Layout.createSequentialGroup()
                .addGroup(panelNhomHang2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNhomHang2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(labelTitle2, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelNhomHang2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelNhomHang2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioDiaPhim, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(radioTatCa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(radioDiaNhac, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(radioSach, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        panelNhomHang2Layout.setVerticalGroup(
            panelNhomHang2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNhomHang2Layout.createSequentialGroup()
                .addComponent(labelTitle2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioTatCa, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioDiaNhac, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioDiaPhim, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSach, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelLocLayout = new javax.swing.GroupLayout(panelLoc);
        panelLoc.setLayout(panelLocLayout);
        panelLocLayout.setHorizontalGroup(
            panelLocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelNhomHang2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelLocLayout.setVerticalGroup(
            panelLocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocLayout.createSequentialGroup()
                .addComponent(panelTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelNhomHang2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel1.setText("Số lượng");

        jLabel3.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel3.setText("Giá bán");

        tfGiaBan.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tfGiaBan.setText("0");

        jButton1.setBackground(new java.awt.Color(255, 153, 0));
        jButton1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/plus_icon.png"))); // NOI18N
        jButton1.setText("Thêm");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelAvatar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                            .addComponent(tfGiaBan)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(spSoLuong)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spSoLuong, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfGiaBan, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelMain.setBackground(new java.awt.Color(255, 255, 255));

        panelBaseInfo.setBackground(new java.awt.Color(255, 255, 255));
        panelBaseInfo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel4.setText("Ngày lập hóa đơn");

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel5.setText("Mã hóa đơn:");

        jLabel8.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel8.setText("Tên Khách hàng");

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel6.setText("Số Điện thoại khách hàng");

        jLabel9.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel9.setText("Số Email khách hàng");

        jTextField1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jTextField1.setText("Mã hóa đơn tự sinh");
        jTextField1.setEnabled(false);

        tfTenKhachHang.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        tfSDT.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        tfEmail.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        javax.swing.GroupLayout panelBaseInfoLayout = new javax.swing.GroupLayout(panelBaseInfo);
        panelBaseInfo.setLayout(panelBaseInfoLayout);
        panelBaseInfoLayout.setHorizontalGroup(
            panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                    .addComponent(tfTenKhachHang, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfSDT)
                    .addComponent(tfEmail))
                .addContainerGap())
        );
        panelBaseInfoLayout.setVerticalGroup(
            panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseInfoLayout.createSequentialGroup()
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addComponent(tfTenKhachHang))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfSDT, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBaseInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelChiTiet.setBackground(new java.awt.Color(255, 255, 255));

        tableChiTiet.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        tableChiTiet.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã sản phẩm", "Tên sản phẩm", "Đơn giá", "Số lượng", "Thành tiền"
            }
        ));
        tableChiTiet.setRowHeight(25);
        jScrollPane2.setViewportView(tableChiTiet);

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 51, 102));
        jLabel2.setText("Tổng tiền hàng");

        lbTongTien.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongTien.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongTien.setText("0");
        lbTongTien.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));

        jLabel10.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 51, 102));
        jLabel10.setText("Tổng sản phẩm");

        lbTongSP.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lbTongSP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbTongSP.setText("0");
        lbTongSP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));

        javax.swing.GroupLayout panelChiTietLayout = new javax.swing.GroupLayout(panelChiTiet);
        panelChiTiet.setLayout(panelChiTietLayout);
        panelChiTietLayout.setHorizontalGroup(
            panelChiTietLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChiTietLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbTongSP, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbTongTien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        panelChiTietLayout.setVerticalGroup(
            panelChiTietLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChiTietLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelChiTietLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbTongSP, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 64, Short.MAX_VALUE))
        );

        btSave.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/save.png"))); // NOI18N
        btSave.setText("Lưu hóa đơn");
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveActionPerformed(evt);
            }
        });

        btHuy.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btHuy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Delete.png"))); // NOI18N
        btHuy.setText("Hủy");
        btHuy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHuyActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btHuy, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelBaseInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelChiTiet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelChiTiet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(panelBaseInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btSave)
                            .addComponent(btHuy)))))
        );

        tableListSP.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        tableListSP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Mã hàng hóa", "Tên hàng hóa", "Giá bán", "Số lượng tồn"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tableListSP.setToolTipText("");
        tableListSP.setAlignmentX(1.0F);
        tableListSP.setRowHeight(25);
        tableListSP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableListSPMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableListSP);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelLoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 772, Short.MAX_VALUE))
                    .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelLoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(80, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 32, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 671, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        boolean flag = false; // kiểm tra xem sản phẩm đã có trong hóa đơn chưa
        try {
            String maSP = labelAvatar.getName();
            if (maSP == null) {
                JOptionPane.showMessageDialog(null, "Lỗi. Bạn chưa chọn sản phẩm!Vui lòng chọn sản phẩm trước", "Thông báo", JOptionPane.ERROR_MESSAGE);
            } else {
                int soLuong = (int) spSoLuong.getValue();
                int giaBan = Integer.parseInt(tfGiaBan.getText());
                if (soLuong == 0) {
                    JOptionPane.showMessageDialog(null, "Lỗi. Bạn chưa nhập số lượng!", "Thông báo", JOptionPane.ERROR_MESSAGE);
                } else if (soLuong < 0) {
                    JOptionPane.showMessageDialog(null, "Lỗi. Số lượng phải > 0!", "Thông báo", JOptionPane.ERROR_MESSAGE);
                } else if (giaBan == 0) {
                    JOptionPane.showMessageDialog(null, "Lỗi. Bạn chưa nhập giá bán!", "Thông báo", JOptionPane.ERROR_MESSAGE);
                } else if (giaBan < 0) {
                    JOptionPane.showMessageDialog(null, "Lỗi. Giá bán phải > 0!", "Thông báo", JOptionPane.ERROR_MESSAGE);
                } else {
                    DefaultTableModel model = (DefaultTableModel) tableChiTiet.getModel();
                    Object obj = frMain.model.getByKey(maSP);
                    if (obj != null && (obj instanceof BaseProduct)) {
                        BaseProduct pro = (BaseProduct) obj;
                        if (pro.getSoluong() < soLuong) {
                            JOptionPane.showMessageDialog(null, "Lỗi. Tồn kho không đủ!", "Thông báo", JOptionPane.ERROR_MESSAGE);
                        } else {
                            for (int i = 0; i < model.getRowCount(); i++) {
                                maSP = tableChiTiet.getValueAt(i, 0).toString();
                                int donGia = Integer.parseInt(tableChiTiet.getValueAt(i, 2).toString());
                                if (maSP.equals(pro.getMaSP()) == true && donGia == giaBan) {
                                    int oldSL = Integer.parseInt(tableChiTiet.getValueAt(i, 3).toString());
                                    tableChiTiet.setValueAt(oldSL + soLuong, i, 3);
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag == false) {
                                model.addRow(new Object[]{pro.getMaSP(), pro.getTenSP(), giaBan, soLuong, soLuong * giaBan});
                            }
                            tongSP += soLuong;
                            tongTien += soLuong * giaBan;
                            lbTongSP.setText(tongSP + "");
                            lbTongTien.setText(tongTien + "");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FormAddPhieuNhap.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, "Lỗi. Bạn chưa nhập số lượng hoặc giá nhập|\nVui lòng nhập đầy đủ thông tin", "Thông báo", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tableListSPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableListSPMouseClicked
        // TODO add your handling code here:
        String maSP = tableListSP.getValueAt(tableListSP.getSelectedRow(), 0).toString();
        Object obj = frMain.model.getByKey(maSP);
        if (obj != null) {
            BaseProduct pro = (BaseProduct) obj;
            ImageProcess.setLabelIcon(labelAvatar, pro.getLinkAvatar(), 253, 144);
            labelAvatar.setName(maSP);
            String giaBan = tableListSP.getValueAt(tableListSP.getSelectedRow(), 2).toString();
            tfGiaBan.setText(giaBan);
        }
    }//GEN-LAST:event_tableListSPMouseClicked

    private void btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveActionPerformed
        // TODO add your handling code here:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = jDateChooser1.getDate();
        String ngayLap = dateFormat.format(date);
        String tenKH = tfTenKhachHang.getText();
        String sdt = tfSDT.getText();
        String email = tfEmail.getText();
        if (tableChiTiet.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "Lỗi. Hóa đơn rỗng!", "Thông báo", JOptionPane.ERROR_MESSAGE);
        } else if (ngayLap == null || ngayLap.equals("") == true) {
            JOptionPane.showMessageDialog(null, "Lỗi. Chưa nhập ngày lập hóa đơn!", "Thông báo", JOptionPane.ERROR_MESSAGE);
        } else if (tenKH == null || tenKH.equals("") == true) {
            JOptionPane.showMessageDialog(null, "Lỗi. Chưa nhập tên khách hàng!", "Thông báo", JOptionPane.ERROR_MESSAGE);
        } else {
            HoaDon hoaDon = new HoaDon(tenKH, sdt, email, ngayLap);
            DefaultTableModel model = (DefaultTableModel) tableChiTiet.getModel();
            for (int i = 0; i < tableChiTiet.getRowCount(); i++) {
                ArrayList<String> arr_ChiTiet = new ArrayList<>();
                arr_ChiTiet.add(tableChiTiet.getValueAt(i, 0).toString()); // mã sp
                arr_ChiTiet.add(tableChiTiet.getValueAt(i, 2).toString()); // đơng giá
                arr_ChiTiet.add(tableChiTiet.getValueAt(i, 3).toString()); // số lượng
                ChiTiet chiTiet = new ChiTiet(arr_ChiTiet);
                hoaDon.addChiTiet(chiTiet);
            }

            frMain.model.add(hoaDon);
            JOptionPane.showMessageDialog(null, "Thêm hóa đơn thành công!", "Thông báo", JOptionPane.INFORMATION_MESSAGE);
            resetTableData(tableChiTiet);
            tfTenKhachHang.setText("");
            tfSDT.setText("");
            tfEmail.setText("");
            lbTongSP.setText("0");
            lbTongTien.setText("0");
            this.tongSP = 0;
            this.tongTien = 0;
            frMain.UpdateAll();
            FormInHoaDon frInHD = new FormInHoaDon(frMain, hoaDon);
        }

    }//GEN-LAST:event_btSaveActionPerformed

    private void LocNhomSP(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LocNhomSP
        // TODO add your handling code here:
        LocNhomSP();
    }//GEN-LAST:event_LocNhomSP

    private void btHuyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHuyActionPerformed
        // TODO add your handling code here:
        resetTableData(tableChiTiet);
        tfSDT.setText("");
        tfEmail.setText("");
        lbTongSP.setText("0");
        lbTongTien.setText("0");
        this.tongSP = 0;
        this.tongTien = 0;
    }//GEN-LAST:event_btHuyActionPerformed

    private void tfSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfSearchMouseClicked
        // TODO add your handling code here:
        tfSearch.selectAll();
    }//GEN-LAST:event_tfSearchMouseClicked

    private void tfSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            TimKiem(tfSearch.getText());
        }
    }//GEN-LAST:event_tfSearchKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btHuy;
    private javax.swing.JButton btSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel labelAvatar;
    private javax.swing.JLabel labelSearch;
    private javax.swing.JLabel labelTitle2;
    private javax.swing.JLabel lbTongSP;
    private javax.swing.JLabel lbTongTien;
    private javax.swing.JPanel panelBaseInfo;
    private javax.swing.JPanel panelChiTiet;
    private javax.swing.JPanel panelLoc;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelNhomHang2;
    private javax.swing.JPanel panelTimKiem;
    private javax.swing.JRadioButton radioDiaNhac;
    private javax.swing.JRadioButton radioDiaPhim;
    private javax.swing.JRadioButton radioSach;
    private javax.swing.JRadioButton radioTatCa;
    private javax.swing.JSpinner spSoLuong;
    private javax.swing.JTable tableChiTiet;
    private javax.swing.JTable tableListSP;
    private javax.swing.JTextField tfEmail;
    private javax.swing.JTextField tfGiaBan;
    private javax.swing.JTextField tfSDT;
    private javax.swing.JTextField tfSearch;
    private javax.swing.JTextField tfTenKhachHang;
    // End of variables declaration//GEN-END:variables

    @Override
    public void TimKiem(String textSearch) {
        DefaultTableModel model = (DefaultTableModel) tableListSP.getModel();
        for (int i = (model.getRowCount() - 1); i >= 0; i--) {
            String maSP = tableListSP.getValueAt(i, 0).toString();
            String tenSP = tableListSP.getValueAt(i, 1).toString();
            if (maSP.equalsIgnoreCase(textSearch) == false && tenSP.contains(textSearch) == false && tenSP.equalsIgnoreCase(textSearch) == false) {
                model.removeRow(i);
            }
        }
    }

    @Override
    public void LocNhomSP() {
        if (radioTatCa.isSelected()) {
            nhomSP = "ALL";
        } else if (radioDiaNhac.isSelected()) {
            nhomSP = "NHAC";
        } else if (radioDiaPhim.isSelected()) {
            nhomSP = "PHIM";
        } else if (radioSach.isSelected()) {
            nhomSP = "SACH";
        }
        setTableData();
    }
}
