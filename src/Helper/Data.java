/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import Config.Config;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Model;
import product.ChiPhiPhatSinh;
import product.ChiTiet;
import product.DiaNhac;
import product.DiaPhim;
import product.HoaDon;
import product.PhieuNhap;
import product.Sach;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class Data {

    static public ArrayList<ArrayList<String>> getData(String data_path) {
        ArrayList<ArrayList<String>> arr_data = new ArrayList<>();
        // Lấy file data
        File data = null;
        data = new File(data_path);
        // Nếu file tồn tại thì đọc dữ liện
        if (data.exists()) {
            FileInputStream fi = null;
            try {
                fi = new FileInputStream(data);
                InputStreamReader isr = new InputStreamReader(fi, "UTF-16");
                BufferedReader br = new BufferedReader(isr);
                String s;
                //Đọc từng dòng văn bản!
                while ((s = br.readLine()) != null) {
                    // bỏ qua chú thích đặt trong {}
                    if (s.equals("{") == true) {
                        while ((s = br.readLine()) != null && s.equals("}") == false);
                        s = br.readLine();
                    }
                    arr_data.add(getArrLi(s, "[|]"));
                }
            } catch (Exception ex) {
                Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fi.close();
                } catch (IOException ex) {
                    Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            System.out.println("File \"" + data_path + "\" không tồn tại");

        }

        return arr_data;
    }

    public static ArrayList<String> getArrLi(String s, String charList) {
        String[] arr = s.split(charList);
        ArrayList<String> arrLi = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            arrLi.add(arr[i]);
        }
        return arrLi;
    }

    public static String getComment(String data_path) {
        File data = null;
        String comment = "";
        data = new File(data_path);
        // Nếu file tồn tại thì đọc dữ liện
        // Comment ở đầu File có dạng
        // {
        //   Nội dung chú thích
        // }
        if (data.exists()) {
            FileInputStream fi = null;
            try {
                fi = new FileInputStream(data);
                InputStreamReader isr = new InputStreamReader(fi, "UTF-16");
                BufferedReader br = new BufferedReader(isr);
                String s = "";
                while ((s = br.readLine()) != null) {
                    // bỏ qua chú thích đặt trong {}
                    if (s.equals("{") == true) {
                        comment += s + "\n";
                        while ((s = br.readLine()) != null && s.equals("}") == false) {
                            comment += s + "\n";
                        }
                        if (s.equals("}") == true) {
                            comment += s + "\n";
                        }
                    }
                }

            } catch (IOException ex) {
                Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (fi != null) {
                        fi.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return comment;
    }

    public static void saveData(Model model) throws IOException {
        String comment = "";

        File file = new File(Config.FILE_DATA_NHAC_PATH);
        File fileChiTiet = null;
        FileOutputStream fo = null, fo1 = null;
        OutputStreamWriter osw = null, osw1 = null;
        BufferedWriter bw = null, bw1 = null;
        String[] str, str1;
        try {
            comment = getComment(Config.FILE_DATA_NHAC_PATH);
            file = new File(Config.FILE_DATA_NHAC_PATH);
            if (file.exists()) {
                fo = new FileOutputStream(file);
                osw = new OutputStreamWriter(fo, "UTF-16");
                bw = new BufferedWriter(osw);
                bw.write(comment);
                for (Map.Entry<String, DiaNhac> entrySet : model.getListDiaNhac().entrySet()) {
                    //<Mã sản phẩm>|<Tên đĩa nhạc>|<Thể loại>|<Ca sĩ, Ca sĩ, ....>|<Bài hát, Bài hát,...>|<Số lượng đĩa>|<Giá bán>|<Tên ảnh đại diện>
                    DiaNhac dn = entrySet.getValue();
                    bw.write(dn.getMaSP() + "|" + dn.getTenSP() + "|" + dn.getTheLoai() + "|");
                    if(dn.getListCasi().size() == 0) bw.write("|");
                    for (int i = 0; i < dn.getListCasi().size(); i++) {
                        if (i == (dn.getListCasi().size() - 1)) {
                            bw.write(dn.getListCasi().get(i) + "|");
                        } else {
                            bw.write(dn.getListCasi().get(i) + ",");
                        }
                    }
                    if(dn.getListBaiHat().size() == 0) bw.write("|");
                    for (int i = 0; i < dn.getListBaiHat().size(); i++) {
                        if (i == (dn.getListBaiHat().size() - 1)) {
                            bw.write(dn.getListBaiHat().get(i) + "|");
                        } else {
                            bw.write(dn.getListBaiHat().get(i) + ",");
                        }
                    }
                    bw.write(dn.getSoluong() + "|");
                    bw.write(dn.getGiaBan() + "|");
                    str = dn.getLinkAvatar().split("[\\\\]");
                    bw.write(str[str.length - 1] + "\n");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bw != null) {
                bw.close();
            }
            if (osw != null) {
                osw.close();
            }
            if (fo != null) {
                fo.close();
            }
        }

        try {
            comment = getComment(Config.FILE_DATA_PHIM_PATH);
            file = new File(Config.FILE_DATA_PHIM_PATH);
            if (file.exists()) {
                fo = new FileOutputStream(file);
                osw = new OutputStreamWriter(fo, "UTF-16");
                bw = new BufferedWriter(osw);
                bw.write(comment);
                for (Map.Entry<String, DiaPhim> entrySet : model.getListDiaPhim().entrySet()) {
                    //<Mã sản phẩm>|<Tên đĩa phim>|<Thể loại>|<Đạo diễn>|<Diễn viên, Diễn viên,...>|<Tên phim, Tên phim, ....>|<Số lượng đĩa>|<Giá bán>|<Tên ảnh đại diện>
                    DiaPhim dp = entrySet.getValue();
                    bw.write(dp.getMaSP() + "|" + dp.getTenSP() + "|" + dp.getTheLoai() + "|" + dp.getDaoDien() + "|");
                    if(dp.getListDienVien().size() == 0) bw.write("|");
                    for (int i = 0; i < dp.getListDienVien().size(); i++) {
                        if (i == (dp.getListDienVien().size() - 1)) {
                            bw.write(dp.getListDienVien().get(i) + "|");
                        } else {
                            bw.write(dp.getListDienVien().get(i) + ",");
                        }
                    }
                    if(dp.getListPhim().size() == 0) bw.write("|");
                    for (int i = 0; i < dp.getListPhim().size(); i++) {
                        if (i == (dp.getListPhim().size() - 1)) {
                            bw.write(dp.getListPhim().get(i) + "|");
                        } else {
                            bw.write(dp.getListPhim().get(i) + ",");
                        }
                    }
                    bw.write(dp.getSoluong() + "|");
                    bw.write(dp.getGiaBan() + "|");
                    str = dp.getLinkAvatar().split("[\\\\]");
                    bw.write(str[str.length - 1] + "\n");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bw != null) {
                bw.close();
            }
            if (osw != null) {
                osw.close();
            }
            if (fo != null) {
                fo.close();
            }
        }

        try {
            comment = getComment(Config.FILE_DATA_SACH_PATH);
            file = new File(Config.FILE_DATA_SACH_PATH);
            if (file.exists()) {
                fo = new FileOutputStream(file);
                osw = new OutputStreamWriter(fo, "UTF-16");
                bw = new BufferedWriter(osw);
                bw.write(comment);
                for (Map.Entry<String, Sach> entrySet : model.getListSach().entrySet()) {
                    //<Mã sản phẩm>|<Tên sách>|<Tác giả, Tác giả, ...>|<NXB>|<Năm XB>|<Số trang>|<Số lượng sách>|<Giá bán>|<Tên ảnh đại diện>
                    Sach sa = entrySet.getValue();
                    bw.write(sa.getMaSP() + "|" + sa.getTenSP() + "|");
                    if(sa.getTacGia().size() == 0) bw.write("|");
                    for (int i = 0; i < sa.getTacGia().size(); i++) {
                        if (i == (sa.getTacGia().size() - 1)) {
                            bw.write(sa.getTacGia().get(i) + "|");
                        } else {
                            bw.write(sa.getTacGia().get(i) + ",");
                        }
                    }
                    bw.write(sa.getNhaXB() + "|");
                    bw.write(sa.getNamXB() + "|");
                    bw.write(sa.getSoTrang() + "|");
                    bw.write(sa.getSoluong() + "|");
                    bw.write(sa.getGiaBan() + "|");
                    str = sa.getLinkAvatar().split("[\\\\]");
                    bw.write(str[str.length - 1] + "\n");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bw != null) {
                bw.close();
            }
            if (osw != null) {
                osw.close();
            }
            if (fo != null) {
                fo.close();
            }
        }

        try {
            comment = getComment(Config.FILE_DATA_PHIEUNHAP_PATH);
            file = new File(Config.FILE_DATA_PHIEUNHAP_PATH);
            if (file.exists()) {
                fo = new FileOutputStream(file);
                osw = new OutputStreamWriter(fo, "UTF-16");
                bw = new BufferedWriter(osw);
                bw.write(comment);
                for (Map.Entry<String, PhieuNhap> entrySet : model.getListPhieuNhap().entrySet()) {
                    //<Mã phiếu nhập>|<Tên nhà cung cấp>|<Trạng thái>|<Ngày nhập>
                    PhieuNhap pn = entrySet.getValue();
                    bw.write(pn.getMa() + "|");
                    bw.write(pn.getTenNhaCungCap() + "|");
                    bw.write(pn.getTrangThaiNhap() + "|");
//                    if (pn.getTrangThaiNhap().equalsIgnoreCase("đã nhập") == true) {
//                        bw.write("YES|");
//                    } else {
//                        bw.write("NO|");
//                    }
                    bw.write(pn.getNgayLap()+ "\n");
                    // Lưu chi tiết phiếu nhập
                    try {
                        if (fileChiTiet == null) {
                            comment = getComment(Config.FILE_DATA_CHITIET_PHIEUNHAP_PATH);
                            fileChiTiet = new File(Config.FILE_DATA_CHITIET_PHIEUNHAP_PATH);
                            if (fileChiTiet.exists()) {
                                fo1 = new FileOutputStream(fileChiTiet);
                                osw1 = new OutputStreamWriter(fo1, "UTF-16");
                                bw1 = new BufferedWriter(osw1);
                                bw1.write(comment);
                            }
                        }
                        if (fileChiTiet.exists()) {
                            for (int i = 0; i < pn.getListChiTiet().size(); i++) {
                                // <Mã phiếu nhập>|<Mã sản phẩm>|<Đơn giá>|<Số lượng>
                                ChiTiet ct = pn.getListChiTiet().get(i);
                                bw1.write(pn.getMa() + "|");
                                bw1.write(ct.getMaSP()+ "|");
                                bw1.write(ct.getDonGia() + "|");
                                bw1.write(ct.getSoLuong() + "\n");
                            }
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bw != null) {
                bw.close();
            }
            if (osw != null) {
                osw.close();
            }
            if (fo != null) {
                fo.close();
            }
            if (bw1 != null) {
                bw1.close();
            }
            if (osw1 != null) {
                osw1.close();
            }
            if (fo1 != null) {
                fo1.close();
            }
            fileChiTiet = null;
        }

        try {
            comment = getComment(Config.FILE_DATA_HOADON_PATH);
            file = new File(Config.FILE_DATA_HOADON_PATH);
            if (file.exists()) {
                fo = new FileOutputStream(file);
                osw = new OutputStreamWriter(fo, "UTF-16");
                bw = new BufferedWriter(osw);
                bw.write(comment);
                for (Map.Entry<String, HoaDon> entrySet : model.getListHoaDon().entrySet()) {
                    //<Mã hóa đơn>|<Ngày lập>|<Tên khách hàng>|<Số điện thoại khách hàng>|<Email khách hàng>
                    HoaDon hd = entrySet.getValue();
                    bw.write(hd.getMa()+ "|");    
                    bw.write(hd.getTenKhachHang()+ "|");
                    bw.write(hd.getSdtKhachHang() + "|");
                    bw.write(hd.getEmailKhachHang() + "|");
                    bw.write(hd.getNgayLap() + "\n");
                    // Lưu chi tiết phiếu nhập
                    try {
                        if (fileChiTiet == null) {
                            comment = getComment(Config.FILE_DATA_CHITIET_HOADON_PATH);
                            fileChiTiet = new File(Config.FILE_DATA_CHITIET_HOADON_PATH);
                            if (fileChiTiet.exists()) {
                                fo1 = new FileOutputStream(fileChiTiet);
                                osw1 = new OutputStreamWriter(fo1, "UTF-16");
                                bw1 = new BufferedWriter(osw1);
                                bw1.write(comment);
                            }
                        }
                        if (fileChiTiet.exists()) {
                            for (int i = 0; i < hd.getListChiTiet().size(); i++) {
                                // <Mã phiếu nhập>|<Mã sản phẩm>|<Đơn giá>|<Số lượng>
                                ChiTiet ct = hd.getListChiTiet().get(i);
                                bw1.write(hd.getMa() + "|");
                                bw1.write(ct.getMaSP()+ "|");
                                bw1.write(ct.getDonGia() + "|");
                                bw1.write(ct.getSoLuong() + "\n");
                            }
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bw != null) {
                bw.close();
            }
            if (osw != null) {
                osw.close();
            }
            if (fo != null) {
                fo.close();
            }
            if (bw1 != null) {
                bw1.close();
            }
            if (osw1 != null) {
                osw1.close();
            }
            if (fo1 != null) {
                fo1.close();
            }
        }

        try {
            comment = getComment(Config.FILE_DATA_CHI_PHI_PHAT_SINH_PATH);
            file = new File(Config.FILE_DATA_CHI_PHI_PHAT_SINH_PATH);
            if (file.exists()) {
                fo = new FileOutputStream(file);
                osw = new OutputStreamWriter(fo, "UTF-16");
                bw = new BufferedWriter(osw);
                bw.write(comment);
                for (ChiPhiPhatSinh cpps : model.getListCPPS()) {
                    //<Ngày>|<Tên chi phí>|<Nội dung chi tiết>|<Tổng tiền>
                    bw.write(cpps.getNgay() + "|");
                    bw.write(cpps.getTen() + "|");
                    bw.write(cpps.getChiTiet() + "|");
                    bw.write(cpps.getTongTien() + "\n");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (bw != null) {
                bw.close();
            }
            if (osw != null) {
                osw.close();
            }
            if (fo != null) {
                fo.close();
            }
        }

    }

    public static String copyFile(String linkFileGoc, String linkFileCopy) {
        String result_linkFileCopy = linkFileCopy;
        InputStream is = null;
        OutputStream os = null;
        try {
            File fileGoc = new File(linkFileGoc);
            File fileCopy = new File(linkFileCopy);
            if (fileCopy.exists() == false) {
                is = new FileInputStream(fileGoc);
                os = new FileOutputStream(fileCopy);
                byte[] buffer = new byte[1024];
                int length;
                // copy the file content in bytes
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                is.close();
                os.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result_linkFileCopy;
    }

    public static void main(String[] args) {
    }
}
