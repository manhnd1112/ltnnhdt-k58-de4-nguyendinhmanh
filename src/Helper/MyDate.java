/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class MyDate {

    public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static Date getCurrentDate() {
        return new Date();
    }
    
    public static String dateParseString(Date date){
        return dateFormat.format(date);
    }

    // định dạng là dd/MM/yyyy
    public static Date getDateFromString(String s) {
        Date date = null;
        try {
            String[] str = s.split("[/-]");
            s = str[0] + "/" + str[1] + "/" + str[2];
            date = dateFormat.parse(s);
        } catch (ParseException ex) {
            Logger.getLogger(MyDate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    // định dạng là YYYY/mm/dd
    // hoặc là: dd/mm/YYYY
    public static int getMonth(String date) {
        String[] arr = date.split("[/-]");
        return Integer.parseInt(arr[1]);
    }
    // dd/mm/YYYY
    public static int getYear(String date) {
        String[] arr = date.split("[/-]");
        return Integer.parseInt(arr[2]);
    }

    public static int getCurrentMonth() {
        return getMonth(dateFormat.format(new Date()));
    }

    public static int getPrevMonth() {
        int curMon = getCurrentMonth();
        if (curMon == 1) {
            return 12;
        } else {
            return (curMon - 1);
        }
    }
    
    public static String datePaserString(Date date){
        return dateFormat.format(date);
    }

    public static int getCurrentYear() {
        return getYear(dateFormat.format(new Date()));
    }

    // chuyển định dạng date: dd/mm/YYYY ===> YYYY/mm/dd
    public static String getDateFormat(String date) {
        String[] arr = date.split("[/-]");
        String dateFormat = arr[2] + "/" + arr[1] + "/" + arr[0];
        return dateFormat;
    }

    public static int compareDate(Date date1, Date date2) {
        if (datePaserString(date1).equals(datePaserString(date2)) == true) {
            return 0; // trùng ngày
        } else if (date1.before(date2) == true) {
            return -1;  // date1 trước date 2
        } else {
            return 1; // date1 sau date2
        }

    }
}
