package image;

import java.awt.Image;
import javax.swing.ImageIcon;

public class MyIcon {
	
	public static ImageIcon resize(int w, int h, int s, String url) { //nen cho thanh static // s = 0 giu chieu rong s=1 giu chieu cao
		ImageIcon icon = new ImageIcon(url);
		if (s==0) {
			h = (int) Math.round((float)(icon.getIconHeight()*w)/icon.getIconWidth());
		} else if (s == 1) {
			w = (int) Math.round((float)(icon.getIconWidth()*h)/icon.getIconHeight());
		}
		Image img = icon.getImage();
		Image newimg = img.getScaledInstance(w, h, java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(newimg);
		return icon;
	}
        
        public static void main(String[] args) {
            
    }
}
