/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package image;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class ImageProcess {

    public static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
        BufferedImage dest = src.getSubimage(0, 0, rect.width, rect.height);
        return dest;
    }

    public static ImageIcon getIconFromFile(int width, int height, File f) {
        BufferedImage image;
        try {
            image = ImageIO.read(f);
//            Rectangle rect = new Rectangle(width, height);
//            image = cropImage(image,rect);
            if (image == null) {
                JOptionPane.showMessageDialog(null, "Dinh dang file khong dung");
                return null;
            }
            int x = width;
            int y = height;
            int ix = image.getWidth();
            int iy = image.getHeight();
            int dx, dy;
            if ((float) x / y >= (float) ix / iy) {
                dy = y;
                float dxf = y * (float) ix / iy;
                dx = Math.round(dxf);
            } else {
                dx = x;
                float dyf = x * (float) iy / ix;
                dy = Math.round(dyf);
            }
            ImageIcon icon = new ImageIcon(image.getScaledInstance(dx, dy, BufferedImage.SCALE_SMOOTH));
            return icon;
        } catch (Exception ex) {
            Logger.getLogger(ImageProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static Image ScaledImage(Image img, int w, int h){
        BufferedImage resizeImage = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizeImage.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(img, 0, 0, w, h, null);
        g2.dispose();
        return resizeImage;
    }
    
    public static void setLabelIcon(JLabel label, String link, int w, int h){
        try {
            File f = new File(link);
            BufferedImage img = ImageIO.read(f);
            ImageIcon icon = new ImageIcon(ScaledImage(img, w, h));
            label.setIcon(icon);
        } catch (IOException ex) {
            Logger.getLogger(ImageProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void setLabelIcon(JLabel label, File f, int w, int h){
        try {
            BufferedImage img = ImageIO.read(f);
            ImageIcon icon = new ImageIcon(ScaledImage(img, w, h));
            label.setIcon(icon);
        } catch (IOException ex) {
            Logger.getLogger(ImageProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    

}
