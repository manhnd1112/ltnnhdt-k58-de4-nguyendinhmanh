/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 * ngày 
 * mô tả
 * chi tiết
 * tổng tiền
 * 
 */
public class ChiPhiPhatSinh {
    private String ngay; 
    private String ten;
    private String chiTiet;
    private int tongTien;
    public ChiPhiPhatSinh(){
    }
    public ChiPhiPhatSinh(ArrayList<String> arr){
        this.ngay = arr.get(0);
        this.ten = arr.get(1);
        this.chiTiet = arr.get(2);
        this.tongTien = Integer.parseInt(arr.get(3));
    }

    public String getNgay() {
        return ngay;
    }

    public void setNgay(String ngay) {
        this.ngay = ngay;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getChiTiet() {
        return chiTiet;
    }

    public void setChiTiet(String chiTiet) {
        this.chiTiet = chiTiet;
    }

    public int getTongTien() {
        return tongTien;
    }

    public void setTongTien(int tongTien) {
        this.tongTien = tongTien;
    }
    
}
