/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import Config.Config;
import Helper.Data;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class Sach extends BaseProduct {
    private ArrayList<String> tacGia = new ArrayList<>();
    private String nhaXB;
    private String namXB;
    private String soTrang;
    public Sach() {}
    public Sach(String maSP, String tenSP, ArrayList<String> tacGia, String nhaXB, String namXB, String soTrang, int soluong, int giaBan, String avatarImageName) {
        this.maSP = maSP;
        this.tenSP = tenSP;
        this.tacGia = tacGia;
        this.nhaXB = nhaXB;
        this.namXB = namXB;
        this.soTrang = soTrang;
        this.soluong = soluong;
        this.giaBan = giaBan;
        this.setLinkAvatar(avatarImageName);
    }
    @Override
    public void setLinkAvatar(String avatarImageName) {
        String link = Config.AVATAR_SACH_PATH + avatarImageName;
        File file = new File(link);
        if (file.exists()) {
            this.linkAvatar = link;
        } else {
            this.linkAvatar = Config.AVATAR_DEFAULT;
        }
    }

    public ArrayList<String> getTacGia() {
        return tacGia;
    }

    public void setTacGia(ArrayList<String> tacGia) {
        this.tacGia = tacGia;
    }

    public String getNhaXB() {
        return nhaXB;
    }

    public void setNhaXB(String nhaXB) {
        this.nhaXB = nhaXB;
    }

    public String getNamXB() {
        return namXB;
    }

    public void setNamXB(String namXB) {
        this.namXB = namXB;
    }

    public String getSoTrang() {
        return soTrang;
    }

    public void setSoTrang(String soTrang) {
        this.soTrang = soTrang;
    }

    

}
