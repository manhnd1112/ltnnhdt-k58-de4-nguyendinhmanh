/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import Config.Config;
import Helper.Data;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class DiaPhim extends BaseProduct{
    private String theLoai;
    private String daoDien;
    private ArrayList<String> listDienVien = new ArrayList<>();
    private ArrayList<String> listPhim = new ArrayList<>();
    public DiaPhim(){
    }
    public DiaPhim(String maSP, String tenSP, String theLoai, String daoDien, ArrayList<String> listDienVien, ArrayList<String> listPhim, int soluong, int giaBan, String avatarImageName){
        this.maSP = maSP;
        this.tenSP = tenSP;
        this.theLoai = theLoai;
        this.daoDien = daoDien;
        this.listDienVien = listDienVien;
        this.listPhim = listPhim;
        this.soluong = soluong;
        this.giaBan = giaBan;
        this.setLinkAvatar(avatarImageName);
    }
    @Override
    public void setLinkAvatar(String avatarImageName) {
        String link = Config.AVATAR_PHIM_PATH + avatarImageName;
        File file = new File(link);
        if(file.exists())
        {
            this.linkAvatar = link;
        }else
            this.linkAvatar = Config.AVATAR_DEFAULT;
    }

    public String getTheLoai() {
        return theLoai;
    }

    public void setTheLoai(String theLoai) {
        this.theLoai = theLoai;
    }

    public String getDaoDien() {
        return daoDien;
    }

    public void setDaoDien(String daoDien) {
        this.daoDien = daoDien;
    }

    public ArrayList<String> getListDienVien() {
        return listDienVien;
    }

    public void setListDienVien(ArrayList<String> listDienVien) {
        this.listDienVien = listDienVien;
    }

    public ArrayList<String> getListPhim() {
        return listPhim;
    }

    public void setListPhim(ArrayList<String> listPhim) {
        this.listPhim = listPhim;
    }
    
    
}
