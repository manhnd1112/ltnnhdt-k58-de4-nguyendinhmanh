/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class PhieuNhap extends PhieuNhapXuat{
    private String tenNhaCungCap;
    private String trangThaiNhap;
    public PhieuNhap(){   
    }
    public PhieuNhap(String tenNhaCungCap, String ngayLap){
        // Dùng khi tạo phiếu nhập bằng giao diện nên sẽ không truyền vào mã
        // Và mặc định khi tạo phiếu nhập mới, trạng thái luôn là Phiếu tạm
        // Muốn Nhập hàng, thì phải dùng chức năng nhập hàng
        this.tenNhaCungCap = tenNhaCungCap;
        this.trangThaiNhap = "Phiếu tạm";
        this.ngayLap = ngayLap;
    }
    public PhieuNhap(String ma, String tenNhaCungCap, String trangThaiNhap, String ngayLap){
        // Dùng khi tạo phiếu nhập lúc đọc từ file dữ liệu
        this.ma = ma;
        this.tenNhaCungCap = tenNhaCungCap;
        this.trangThaiNhap = trangThaiNhap;
        this.ngayLap = ngayLap;
    }

    public String getTenNhaCungCap() {
        return tenNhaCungCap;
    }

    public void setTenNhaCungCap(String tenNhaCungCap) {
        this.tenNhaCungCap = tenNhaCungCap;
    }

    public String getTrangThaiNhap() {
        return trangThaiNhap;
    }

    public void setTrangThaiNhap(String trangThaiNhap) {
        this.trangThaiNhap = trangThaiNhap;
    }
    
    
}
