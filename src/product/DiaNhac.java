/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import Helper.Data;
import Config.Config;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class DiaNhac extends BaseProduct{
    private String theLoai;
    private ArrayList<String> listCasi = new ArrayList<>();
    private ArrayList<String> listBaiHat = new ArrayList<>();
    public DiaNhac(){
    }
    public DiaNhac(String maSP, String tenSP, String theLoai, ArrayList<String> listCasi, ArrayList<String> listBaiHat, int soluong, int giaBan, String avatarImageName){
        this.maSP = maSP;
        this.tenSP = tenSP;
        this.theLoai = theLoai;
        this.listCasi = listCasi;
        this.listBaiHat = listBaiHat;
        this.soluong = soluong;
        this.giaBan = giaBan;
        this.setLinkAvatar(avatarImageName);
    }
    
    @Override
    public void setLinkAvatar(String avatarImageName) {
        String link = Config.AVATAR_NHAC_PATH + avatarImageName;
        File file = new File(link);
        if(file.exists())
        {
            this.linkAvatar = link;
        }else
            this.linkAvatar = Config.AVATAR_DEFAULT;
    }

    public String getTheLoai() {
        return theLoai;
    }

    public void setTheLoai(String theLoai) {
        this.theLoai = theLoai;
    }

    public ArrayList<String> getListCasi() {
        return listCasi;
    }

    public void setListCasi(ArrayList<String> listCasi) {
        this.listCasi = listCasi;
    }

    public ArrayList<String> getListBaiHat() {
        return listBaiHat;
    }

    public void setListBaiHat(ArrayList<String> listBaiHat) {
        this.listBaiHat = listBaiHat;
    }

   
    
}
