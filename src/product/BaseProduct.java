/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import Config.Config;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 * 
 */
public abstract class BaseProduct {
    protected String maSP; // Tự sinh
    protected String tenSP;
    protected int soluong = 0;
    protected int giaBan = 0;
    protected String linkAvatar;
    public abstract  void setLinkAvatar(String avatarImageName);
    

    public String getMaSP() {
        return maSP;
    }

    public String getLinkAvatar() {
        return linkAvatar;
    }

    public void setMaSP(String maSP) {
        this.maSP = maSP;
    }

    public String getTenSP() {
        return tenSP;
    }

    public void setTenSP(String tenSP) {
        this.tenSP = tenSP;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public int getGiaBan() {
        return giaBan;
    }

    public void setGiaBan(int giaBan) {
        this.giaBan = giaBan;
    }
    
}
