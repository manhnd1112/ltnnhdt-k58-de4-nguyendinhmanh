/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class PhieuNhapXuat {
    protected String ma; // mã phiếu nhập hoặc mã hóa đơn
    protected String ngayLap; // ngày lập phiếu nhập hoặc hóa đơn
    protected ArrayList<ChiTiet> listChiTiet = new ArrayList<>();

    public void addChiTiet(ChiTiet chiTiet) {
        listChiTiet.add(chiTiet);
    }

    public void removeChiTiet(String key) {
        for (int i = (listChiTiet.size() - 1); i >= 0; i--) {
            ChiTiet ct = listChiTiet.get(i);
            if (ct.getMaSP().equals(key) == true) {
                listChiTiet.remove(i);
            }
        }
    }

    public int tongTien() {
        int tong = 0;
        for (int i = 0; i < listChiTiet.size(); i++) {
            tong += listChiTiet.get(i).getDonGia() * listChiTiet.get(i).getSoLuong();
        }
        return tong;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getNgayLap() {
        return ngayLap;
    }

    public void setNgayLap(String ngayLap) {
        this.ngayLap = ngayLap;
    }

    public ArrayList<ChiTiet> getListChiTiet() {
        return listChiTiet;
    }

    public void setListChiTiet(ArrayList<ChiTiet> listChiTiet) {
        this.listChiTiet = listChiTiet;
    }
    
    
}
