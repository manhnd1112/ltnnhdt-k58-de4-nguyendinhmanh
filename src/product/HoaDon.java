/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class HoaDon extends PhieuNhapXuat{
    private String tenKhachHang;
    private String sdtKhachHang;   // nếu có
    private String emailKhachHang; // nếu co
    
    public HoaDon(){
    }
    public HoaDon(String tenKhachHang, String sdtKhachHang, String emailKhachHang, String ngayLap){
        // Dùng khi tạo hóa đơn bằng giao diện nên sẽ không truyền vào mã
        this.tenKhachHang = tenKhachHang;
        this.sdtKhachHang = sdtKhachHang;
        this.emailKhachHang = emailKhachHang;
        this.ngayLap = ngayLap;
    }
    public HoaDon(String ma, String tenKhachHang, String sdtKhachHang, String emailKhachHang, String ngayLap){
        // Dùng khi tạo phiếu nhập lúc đọc từ file dữ liệu
        this.ma = ma;
        this.tenKhachHang = tenKhachHang;
        this.sdtKhachHang = sdtKhachHang;
        this.emailKhachHang = emailKhachHang;
        this.ngayLap = ngayLap;
    }

    public String getTenKhachHang() {
        return tenKhachHang;
    }

    public void setTenKhachHang(String tenKhachHang) {
        this.tenKhachHang = tenKhachHang;
    }

    public String getSdtKhachHang() {
        return sdtKhachHang;
    }

    public void setSdtKhachHang(String sdtKhachHang) {
        this.sdtKhachHang = sdtKhachHang;
    }

    public String getEmailKhachHang() {
        return emailKhachHang;
    }

    public void setEmailKhachHang(String emailKhachHang) {
        this.emailKhachHang = emailKhachHang;
    }
    
    
}
