/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 * Chi tiết từng sản phẩm trong Phiếu nhập hoặc Hóa đơn
 */
public class ChiTiet {
    private String maSP;
    private int donGia;
    private int soLuong;
    public ChiTiet(ArrayList<String> arr){
        this.maSP = arr.get(0);
        this.donGia = Integer.parseInt(arr.get(1));
        this.soLuong = Integer.parseInt(arr.get(2));
    }

    public String getMaSP() {
        return maSP;
    }

    public void setMaSP(String maSP) {
        this.maSP = maSP;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }
    
    
}
