/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

import Helper.Data;
import Encryption.MD5;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class Config {
    public final static String APP_PATH = System.getProperty("user.dir")+"\\src\\";
    public final static String FILE_TAI_KHOAN_ADMIN = APP_PATH + "data\\TaiKhoanAdmin.txt";
    public final static String FILE_DATA_NHAC_PATH = APP_PATH + "data\\Nhac.txt";
    public final static String FILE_DATA_PHIM_PATH = APP_PATH + "data\\Phim.txt";
    public final static String FILE_DATA_SACH_PATH = APP_PATH + "data\\Sach.txt";
    public final static String FILE_DATA_PHIEUNHAP_PATH = APP_PATH + "data\\PhieuNhap.txt";
    public final static String FILE_DATA_CHITIET_PHIEUNHAP_PATH = APP_PATH + "data\\ChiTietPhieuNhap.txt";
    public final static String FILE_DATA_HOADON_PATH = APP_PATH + "data\\HoaDon.txt";
    public final static String FILE_DATA_CHITIET_HOADON_PATH = APP_PATH + "data\\ChiTietHoaDon.txt";
    public final static String FILE_DATA_CHI_PHI_PHAT_SINH_PATH = APP_PATH + "data\\ChiPhiPhatSinh.txt";
    public final static String BACKGROUND_IMAGE_URL = APP_PATH + "image\\background.jpg";
    public final static String AVATAR_NHAC_PATH = APP_PATH + "image\\Avatar\\nhac\\";
    public final static String AVATAR_PHIM_PATH = APP_PATH + "image\\Avatar\\phim\\";
    public final static String AVATAR_SACH_PATH = APP_PATH + "image\\Avatar\\sach\\";
    public final static String AVATAR_DEFAULT = APP_PATH + "image\\none.png";
    private static String USERNAME_ADMIN = getUSERNAME_ADMIN();
    private static String PASSWORD_ADMIN = getPASSWORD_ADMIN();
    public final static String windowsClassName = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    
    public Config(){
        ArrayList<ArrayList<String>> arr = Data.getData(FILE_TAI_KHOAN_ADMIN);
        USERNAME_ADMIN = arr.get(0).get(0);
        PASSWORD_ADMIN = arr.get(1).get(0);
    }
    public static String getUSERNAME_ADMIN() {
        ArrayList<ArrayList<String>> arr = Data.getData(FILE_TAI_KHOAN_ADMIN);
        USERNAME_ADMIN = arr.get(0).get(0);
        return USERNAME_ADMIN;
    }

    public static void setUSERNAME_ADMIN(String USERNAME_ADMIN) {
        Config.USERNAME_ADMIN = USERNAME_ADMIN;
    }

    public static String getPASSWORD_ADMIN() {
        ArrayList<ArrayList<String>> arr = Data.getData(FILE_TAI_KHOAN_ADMIN);
        PASSWORD_ADMIN = arr.get(1).get(0);
        return PASSWORD_ADMIN;
    }

    public static void setPASSWORD_ADMIN(String PASSWORD_ADMIN) {
        Config.PASSWORD_ADMIN = PASSWORD_ADMIN;
    }
    
    public static void main(String[] args) {
    }

    
    
}
