/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Config.Config;
import Helper.Data;
import Helper.MyDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import product.BaseProduct;
import product.ChiPhiPhatSinh;
import product.ChiTiet;
import product.DiaNhac;
import product.DiaPhim;
import product.HoaDon;
import product.PhieuNhap;
import product.Sach;

/**
 *
 * @author Nguyen Dinh Manh listPro : chứa tất cả sản phẩm: đĩa nhạc,
 * đĩa phim, sách --> để phục vụ cho việc tiếm dễ hơn listDiaNhac: chứa
 * các sản phẩm đĩa nhạc listDiaPhim: chứa các sản phẩm đĩa phim
 * listSach: chứa các sản phẩm sách listPhieuNhap: chứa các phiếu nhập
 */
public class Model {

    private HashMap<String, BaseProduct> listPro = new HashMap<>();
    private HashMap<String, DiaNhac> listDiaNhac = new HashMap<>();
    private HashMap<String, DiaPhim> listDiaPhim = new HashMap<>();
    private HashMap<String, Sach> listSach = new HashMap<>();
    private HashMap<String, PhieuNhap> listPhieuNhap = new HashMap<>();
    private HashMap<String, HoaDon> listHoaDon = new HashMap<>();
    private ArrayList<ChiPhiPhatSinh> listCPPS = new ArrayList<ChiPhiPhatSinh>();

    public Model() {
        getData();
    }

    public void getData() {
        DiaNhac dn;
        DiaPhim dp;
        Sach sa;
        // Đọc dữ liệu các sản phẩm: đĩa phim, nhạc, sách
        ArrayList<ArrayList<String>> arr = Data.getData(Config.FILE_DATA_NHAC_PATH);
        for (int i = 0; i < arr.size(); i++) {
            ArrayList<String> arr_item = arr.get(i);
            // Arr theo thứ tự
            // <Mã sản phẩm>|<Tên đĩa nhạc>|<Thể loại>|<Ca sĩ, Ca sĩ, ....>|<Bài hát, Bài hát,...>|<Số lượng đĩa>|<Giá bán>|<Tên ảnh đại diện>
            dn = new DiaNhac(arr_item.get(0), arr_item.get(1), arr_item.get(2), Data.getArrLi(arr_item.get(3), "[,]"), Data.getArrLi(arr_item.get(4), "[,]"), Integer.parseInt(arr_item.get(5)), Integer.parseInt(arr_item.get(6)), arr_item.get(7));
            listDiaNhac.put(dn.getMaSP(), dn);
            listPro.put(dn.getMaSP(), dn);
        }
        arr = Data.getData(Config.FILE_DATA_PHIM_PATH);
        for (int i = 0; i < arr.size(); i++) {
            ArrayList<String> arr_item = arr.get(i);
            // Arr theo thứ tự
            // <Mã sản phẩm>|<Tên đĩa phim>|<Thể loại>|<Đạo diễn,Đạo diễn,...>|<Diễn viên, Diễn viên,...>|<Tên phim, Tên phim, ....>|<Số lượng đĩa>|<Giá bán>|<Tên ảnh đại diện>
            dp = new DiaPhim(arr_item.get(0), arr_item.get(1), arr_item.get(2), arr_item.get(3), Data.getArrLi(arr_item.get(4), "[,]"), Data.getArrLi(arr_item.get(5), "[,]"), Integer.parseInt(arr_item.get(6)), Integer.parseInt(arr_item.get(7)), arr_item.get(8));
            listDiaPhim.put(dp.getMaSP(), dp);
            listPro.put(dp.getMaSP(), dp);
        }
        arr = Data.getData(Config.FILE_DATA_SACH_PATH);
        for (int i = 0; i < arr.size(); i++) {
            ArrayList<String> arr_item = arr.get(i);
            // Arr theo thứ tự
            // <Mã sản phẩm>|<Tên sách>|<Tác giả, Tác giả, ...>|<NXB>|<Năm XB>|<Số trang>|<Trọng lượng>|<Số lượng sách>|<Giá bán>|<Tên ảnh đại diện>
            sa = new Sach(arr_item.get(0), arr_item.get(1), Data.getArrLi(arr_item.get(2), "[,]"), arr_item.get(3), arr_item.get(4), arr_item.get(5), Integer.parseInt(arr_item.get(6)), Integer.parseInt(arr_item.get(7)), arr_item.get(8));
            listSach.put(sa.getMaSP(), sa);
            listPro.put(sa.getMaSP(), sa);
        }

        // Đọc phiếu nhập
        PhieuNhap phieuNhap;
        ChiTiet chiTiet;
        ArrayList<ArrayList<String>> arr_data = Data.getData(Config.FILE_DATA_PHIEUNHAP_PATH);
        for (int i = 0; i < arr_data.size(); i++) {
            ArrayList<String> arr_item = arr_data.get(i);
            phieuNhap = new PhieuNhap(arr_item.get(0), arr_item.get(1), arr_item.get(2), arr_item.get(3));
            this.listPhieuNhap.put(phieuNhap.getMa(), phieuNhap);
        }
        ArrayList<ArrayList<String>> arr_ChiTiets = Data.getData(Config.FILE_DATA_CHITIET_PHIEUNHAP_PATH);
        for (int i = 0; i < arr_ChiTiets.size(); i++) {
            String maPhieuNhap = arr_ChiTiets.get(i).get(0);
            if (this.listPhieuNhap.containsKey(maPhieuNhap)) {
                ArrayList<String> arr_Info_ChiTiet = new ArrayList<>();
                arr_Info_ChiTiet.add(arr_ChiTiets.get(i).get(1)); // mã sp
                arr_Info_ChiTiet.add(arr_ChiTiets.get(i).get(2)); // đơn giá
                arr_Info_ChiTiet.add(arr_ChiTiets.get(i).get(3)); // số lượng
                chiTiet = new ChiTiet(arr_Info_ChiTiet);
                this.listPhieuNhap.get(maPhieuNhap).addChiTiet(chiTiet);
            }
        }

        //  Đọc hóa đơn
        HoaDon hoaDon;
        arr_data = Data.getData(Config.FILE_DATA_HOADON_PATH);
        for (int i = 0; i < arr_data.size(); i++) {
            ArrayList<String> arr_item = arr_data.get(i);
            hoaDon = new HoaDon(arr_item.get(0), arr_item.get(1), arr_item.get(2), arr_item.get(3), arr_item.get(4));
            this.listHoaDon.put(hoaDon.getMa(), hoaDon);
        }
        arr_ChiTiets = Data.getData(Config.FILE_DATA_CHITIET_HOADON_PATH);
        for (int i = 0; i < arr_ChiTiets.size(); i++) {
            String maHD = arr_ChiTiets.get(i).get(0);
            if (this.listHoaDon.containsKey(maHD)) {
                ArrayList<String> arr_Info_ChiTiet = new ArrayList<>();
                arr_Info_ChiTiet.add(arr_ChiTiets.get(i).get(1)); // mã sp
                arr_Info_ChiTiet.add(arr_ChiTiets.get(i).get(2)); // đơn giá
                arr_Info_ChiTiet.add(arr_ChiTiets.get(i).get(3)); // số lượng
                chiTiet = new ChiTiet(arr_Info_ChiTiet);
                this.listHoaDon.get(maHD).addChiTiet(chiTiet);
            }
        }

        arr = Data.getData(Config.FILE_DATA_CHI_PHI_PHAT_SINH_PATH);
        System.out.println(arr.size());
        for (int i = 0; i < arr.size(); i++) {
            ChiPhiPhatSinh chps = new ChiPhiPhatSinh(arr.get(i));
            listCPPS.add(chps);
        }
    }

    public Object getByKey(String key) {
        Object obj = null;
        if (this.listPro.containsKey(key)) {
            obj = this.listPro.get(key);
        } else if (this.listPhieuNhap.containsKey(key)) {
            obj = this.listPhieuNhap.get(key);
        } else if (this.listHoaDon.containsKey(key)) {
            obj = this.listHoaDon.get(key);
        }
        return obj;
    }

    public void add(Object obj) {
        if (obj instanceof BaseProduct) {
            BaseProduct pro;
            pro = (BaseProduct) obj;
            String maSP;
            if (pro instanceof DiaNhac) {
                for (int i = 1; i < 10000; i++) {
                    maSP = this.getKey(i, pro);
                    if (this.listDiaNhac.containsKey(maSP) == false) {
                        pro.setMaSP(maSP);
                        this.listDiaNhac.put(maSP, (DiaNhac) pro);
                        this.listPro.put(maSP, (BaseProduct) pro);
                        break;
                    }
                }
            }
            if (pro instanceof DiaPhim) {
                for (int i = 1; i < 10000; i++) {
                    maSP = this.getKey(i, pro);
                    if (this.listDiaPhim.containsKey(maSP) == false) {
                        pro.setMaSP(maSP);
                        this.listDiaPhim.put(maSP, (DiaPhim) pro);
                        this.listPro.put(maSP, (BaseProduct) pro);
                        break;
                    }
                }
            }
            if (pro instanceof Sach) {
                for (int i = 1; i < 10000; i++) {
                    maSP = this.getKey(i, pro);
                    if (this.listSach.containsKey(maSP) == false) {
                        pro.setMaSP(maSP);
                        this.listSach.put(maSP, (Sach) pro);
                        this.listPro.put(maSP, (BaseProduct) pro);
                        break;
                    }
                }
            }
        } else if (obj instanceof PhieuNhap) {
            String maPN;
            for (int i = 1; i < 10000; i++) {
                maPN = this.getKey(i, obj);
                if (this.listPhieuNhap.containsKey(maPN) == false) {
                    PhieuNhap phieuNhap = (PhieuNhap) obj;
                    phieuNhap.setMa(maPN);
                    this.listPhieuNhap.put(maPN, phieuNhap);
                    break;
                }
            }
        } else if (obj instanceof HoaDon) {
            String maHD;
            for (int i = 1; i < 10000; i++) {
                maHD = this.getKey(i, obj);
                if (this.listHoaDon.containsKey(maHD) == false) {
                    HoaDon hoaDon = (HoaDon) obj;
                    hoaDon.setMa(maHD);
                    this.listHoaDon.put(maHD, hoaDon);
                    for (ChiTiet chiTiet : hoaDon.getListChiTiet()) {
                        String maSP = chiTiet.getMaSP();
                        int soLuong = chiTiet.getSoLuong();
                        if (this.listPro.containsKey(maSP)) {
                            BaseProduct pro = this.listPro.get(maSP);
                            pro.setSoluong(pro.getSoluong() - soLuong);
                            // update các list SP
                            this.listPro.remove(maSP);
                            this.listPro.put(maSP, pro);
                            if (pro instanceof DiaNhac) {
                                this.listDiaNhac.remove(maSP);
                                this.listDiaNhac.put(maSP, (DiaNhac) pro);
                            } else if (pro instanceof DiaPhim) {
                                this.listDiaPhim.remove(maSP);
                                this.listDiaPhim.put(maSP, (DiaPhim) pro);
                            } else if (pro instanceof Sach) {
                                this.listSach.remove(maSP);
                                this.listSach.put(maSP, (Sach) pro);
                            }
                        }
                    }
                    break;
                }
            }
        } else if (obj instanceof ChiPhiPhatSinh) {
            this.listCPPS.add((ChiPhiPhatSinh) obj);
        }
    }

    public void update(Object obj) {
        if (obj instanceof BaseProduct) {
            BaseProduct pro;
            pro = (BaseProduct) obj;
            String maSP = pro.getMaSP();
            System.out.println(maSP);
            if (pro instanceof DiaNhac) {
                if (this.listDiaNhac.containsKey(maSP)) {
                    this.listDiaNhac.remove(maSP);
                    this.listPro.remove(maSP);
                    this.listDiaNhac.put(maSP, (DiaNhac) pro);
                    this.listPro.put(maSP, (DiaNhac) pro);
                }
            }
            if (pro instanceof DiaPhim) {
                if (this.listDiaPhim.containsKey(maSP)) {
                    this.listDiaPhim.remove(maSP);
                    this.listPro.remove(maSP);
                    this.listDiaPhim.put(maSP, (DiaPhim) pro);
                    this.listPro.put(maSP, (DiaPhim) pro);
                }
            }
            if (pro instanceof Sach) {
                if (this.listSach.containsKey(maSP)) {
                    this.listSach.remove(maSP);
                    this.listPro.remove(maSP);
                    this.listSach.put(maSP, (Sach) pro);
                    this.listPro.put(maSP, (Sach) pro);
                }
            }
        } else if (obj instanceof PhieuNhap) {
            PhieuNhap phieuNhap = (PhieuNhap) obj;
            this.listPhieuNhap.remove(phieuNhap.getMa());
            this.listPhieuNhap.put(phieuNhap.getMa(), phieuNhap);
            ChiTiet chiTiet;
            for (int i = 0; i < phieuNhap.getListChiTiet().size(); i++) {
                chiTiet = phieuNhap.getListChiTiet().get(i);
                String maSP = chiTiet.getMaSP();
                int soLuong = chiTiet.getSoLuong();
                if (this.listPro.containsKey(maSP)) {
                    BaseProduct pro = this.listPro.get(maSP);
                    pro.setSoluong(pro.getSoluong() + soLuong);
                    // update các list SP
                    this.listPro.remove(maSP);
                    this.listPro.put(maSP, pro);
                    if (pro instanceof DiaNhac) {
                        this.listDiaNhac.remove(maSP);
                        this.listDiaNhac.put(maSP, (DiaNhac) pro);
                    } else if (pro instanceof DiaPhim) {
                        this.listDiaPhim.remove(maSP);
                        this.listDiaPhim.put(maSP, (DiaPhim) pro);
                    } else if (pro instanceof Sach) {
                        this.listSach.remove(maSP);
                        this.listSach.put(maSP, (Sach) pro);
                    }
                }
            }
        }
    }

    public boolean delByKey(String key) {
        boolean result = false;
        if (this.listPro.containsKey(key)) {
            BaseProduct pro = this.listPro.get(key);
            if (pro instanceof DiaNhac) {
                this.listDiaNhac.remove(key);
            }
            if (pro instanceof DiaPhim) {
                this.listDiaPhim.remove(key);
            }
            if (pro instanceof Sach) {
                this.listSach.remove(key);
            }
            this.listPro.remove(key);
            for (Map.Entry<String, PhieuNhap> entrySet : listPhieuNhap.entrySet()) {
                PhieuNhap pn = entrySet.getValue();
                pn.removeChiTiet(key);
            }
            for (Map.Entry<String, HoaDon> entrySet : listHoaDon.entrySet()) {
                HoaDon hd = entrySet.getValue();
                hd.removeChiTiet(key);
            }
            result = true;
        } else if (this.listPhieuNhap.containsKey(key)) {
            this.listPhieuNhap.remove(key);
            result = true;
        }
        return result;
    }

    public String getKey(int number, Object obj) {
        String key = "";
        if (obj instanceof DiaNhac) {
            key += "NH";
        }
        if (obj instanceof DiaPhim) {
            key += "PH";
        }
        if (obj instanceof Sach) {
            key += "SA";
        }
        if (obj instanceof PhieuNhap) {
            key += "PN";
        }
        if (obj instanceof HoaDon) {
            key += "HD";
        }
        // chuyển phần số trong mã sang String
        // => Mục đích để đếm số kí tự rồi sinh thêm số 0 ở đầu mã
        // Mặc định 1 mã sản phẩm 4 kí tự thể hiện số
        // Ví dụ: NH0001
        String phanSo = Integer.toString(number);
        for (int i = 0; i < (4 - phanSo.length()); i++) {
            key += '0';
        }
        key += phanSo;
        return key;
    }

    public ArrayList<PhieuNhap> getPhieuNhapByDate(Date date1, Date date2) {
        ArrayList<PhieuNhap> listPN = new ArrayList<>();
        for (Map.Entry<String, PhieuNhap> entrySet : listPhieuNhap.entrySet()) {
            PhieuNhap pn = entrySet.getValue();
            Date date = MyDate.getDateFromString(pn.getNgayLap());
            if (MyDate.compareDate(date1, date) <= 0 && MyDate.compareDate(date2, date) >= 0) {
                listPN.add(pn);
            }
        }
        return listPN;
    }

    public ArrayList<ChiPhiPhatSinh> getCppsByDate(Date date1, Date date2) {
        ArrayList<ChiPhiPhatSinh> listCpps = new ArrayList<>();
        for (ChiPhiPhatSinh cpps : listCPPS) {
            Date date = MyDate.getDateFromString(cpps.getNgay());
            if (MyDate.compareDate(date1, date) <= 0 && MyDate.compareDate(date2, date) >= 0) {
                listCpps.add(cpps);
            }
        }
        return listCpps;
    }

    public ArrayList<HoaDon> getHoaDonByDate(Date date1, Date date2) {
        ArrayList<HoaDon> listHD = new ArrayList<>();
        for (Map.Entry<String, HoaDon> entrySet : listHoaDon.entrySet()) {
            HoaDon hd = entrySet.getValue();
            Date date = MyDate.getDateFromString(hd.getNgayLap());
            if (MyDate.compareDate(date1, date) <= 0 && MyDate.compareDate(date2, date) >= 0) {
                listHD.add(hd);
            }
        }
        return listHD;
    }

    public ArrayList<PhieuNhap> getPhieuNhapInMonth(int month, int year) {
        ArrayList<PhieuNhap> listPN = new ArrayList<>();
        for (Map.Entry<String, PhieuNhap> entrySet : listPhieuNhap.entrySet()) {
            PhieuNhap pn = entrySet.getValue();
            if (MyDate.getMonth(pn.getNgayLap()) == month && MyDate.getYear(pn.getNgayLap()) == year) {
                listPN.add(pn);
            }
        }
        return listPN;
    }

    public ArrayList<HoaDon> getHoaDonInMonth(int month, int year) {
        ArrayList<HoaDon> listHD = new ArrayList<>();
        for (Map.Entry<String, HoaDon> entrySet : listHoaDon.entrySet()) {
            HoaDon hd = entrySet.getValue();
            if (MyDate.getMonth(hd.getNgayLap()) == month && MyDate.getYear(hd.getNgayLap()) == year) {
                listHD.add(hd);
            }
        }
        return listHD;
    }

    public ArrayList<ChiPhiPhatSinh> getCppsInMonth(int month, int year) {
        ArrayList<ChiPhiPhatSinh> listCpps = new ArrayList<>();
        for (ChiPhiPhatSinh cpps : listCPPS) {
            if (MyDate.getMonth(cpps.getNgay()) == month && MyDate.getYear(cpps.getNgay()) == year) {
                listCpps.add(cpps);
            }
        }
        return listCpps;
    }

    public HashMap<String, BaseProduct> getListPro() {
        return listPro;
    }

    public void setListPro(HashMap<String, BaseProduct> listPro) {
        this.listPro = listPro;
    }

    public HashMap<String, DiaNhac> getListDiaNhac() {
        return listDiaNhac;
    }

    public void setListDiaNhac(HashMap<String, DiaNhac> listDiaNhac) {
        this.listDiaNhac = listDiaNhac;
    }

    public HashMap<String, DiaPhim> getListDiaPhim() {
        return listDiaPhim;
    }

    public void setListDiaPhim(HashMap<String, DiaPhim> listDiaPhim) {
        this.listDiaPhim = listDiaPhim;
    }

    public HashMap<String, Sach> getListSach() {
        return listSach;
    }

    public void setListSach(HashMap<String, Sach> listSach) {
        this.listSach = listSach;
    }

    public HashMap<String, PhieuNhap> getListPhieuNhap() {
        return listPhieuNhap;
    }

    public void setListPhieuNhap(HashMap<String, PhieuNhap> listPhieuNhap) {
        this.listPhieuNhap = listPhieuNhap;
    }

    public HashMap<String, HoaDon> getListHoaDon() {
        return listHoaDon;
    }

    public void setListHoaDon(HashMap<String, HoaDon> listHoaDon) {
        this.listHoaDon = listHoaDon;
    }

    public ArrayList<ChiPhiPhatSinh> getListCPPS() {
        return listCPPS;
    }

    public void setListCPPS(ArrayList<ChiPhiPhatSinh> listCPPS) {
        this.listCPPS = listCPPS;
    }

}
